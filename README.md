# Onward

This project is the Onward financial user application.

## Dependencies

- [Angular CLI](https://cli.angular.io/)
- [Firebase CLI](https://firebase.google.com/docs/cli/)
- [Webpack](https://webpack.js.org/)
- [Ionic](https://ionicframework.com/)
- [Cordova](https://cordova.apache.org/)
- [Typescript](https://www.typescriptlang.org/)

## Setup

Clone the repo
Install the dependencies globally `npm install -g (dependency)`.
Run `npm install` in the project directory.
Run `cordova platform add android` to add the android platform.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
Run `ionic run android` to run on connected android device or emulator. Development mode has to be set on the device and the necessary drivers installed.

## Build

Run `ionic build android` to build the project for android and web. The build artifacts will be stored in the `www/` directory. The apk file will be stored in `cordova/platforms/android/build/outputs/apk`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deployment

Run `firebase deploy` to deploy to firebase web hosting and cloud functions.
Run `firebase deploy --except functions` to deploy only hosting
Run `firebase deploy --only functions` to deploy only cloud functions
To deploy to live, run `firebase use {Live environment ID}` first before running any deployment command

