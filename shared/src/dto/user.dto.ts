export interface UserDTO {
    id?: string;
    email: string;
    phone_number: string;
    first_name: string;
    middle_name: string;
    last_name: string;
    role?: "user" | "customer" | "admin";
}