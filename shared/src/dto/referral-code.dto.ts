export interface ReferralCodeDTO {
    id:string;
    referralCode: string;
}