export interface EmployerRequestDTO {
    company: string;
    name: string;
    phone: string;
    email: string;
    location: string;
}