import { Synapse } from "../types/synapse-docs";

export type SynapseLogin = {
    email: string;
    password?: string;
    scope: 'READ_AND_WRITE' | 'READ'
}

export interface SynapseUserCreateDTO {
    logins: Array<SynapseLogin>;
    phone_numbers: Array<string>;
    legal_names: Array<string>;
    documents?: Array<SynapseUserDTO>;
    extra: SynapseUserExtraDTO;
}

export interface SynapseUserExtraDTO {
    supp_id?: string;
    cip_tag: number;
    is_business: boolean;
}

export interface SynapseUserDTO {
    name: string;
    email: string;
    alias?: string;
    phone_number: string;
    ip?: string;
    entity_type: string;
    entity_scope: string;
    day: number;
    month: number;
    year: number;
    address_street: string;
    address_city: string;
    address_subdivision: string;
    address_postal_code: string;
    address_country_code: string;
    virtual_docs?: Array<Synapse.docs>;
    physical_docs?: Array<Synapse.docs>;
    props?;
};