export interface SynapseAuthPostDTO {
    refresh_token: string;
    scope?: Array<string>;
    phone_number?: string;
    validation_pin?: string;
}

export interface SynapseAuthPostResultDTO {
    client_id: string;
    client_name: string;
    expires_at: string;
    expires_in: string;
    oauth_key: string;
    refresh_expires_in: number;
    refresh_token: string;
    scope: Array<string>;
    user_id: string;
}