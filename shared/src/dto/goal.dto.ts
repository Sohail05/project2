export interface GoalDTO {
    gid?: string;
    name?: string;
    deduction: number;
    targetAmount: number;
    targetDate: string;
}