import { User } from './models/user.model';
import { SynapseUserCreateDTO } from './dto/synapse-user.dto';

export class UserParser {

    constructor(public kyc, public user: User) { }

    SynapseUserCreateDTO(): SynapseUserCreateDTO {

        const payload: SynapseUserCreateDTO = {
            logins: [{
                email: this.user.email,
                scope: 'READ_AND_WRITE'
            }],
            phone_numbers: [this.user.phone_number],
            legal_names: [[this.user.first_name, this.user.middle_name, this.user.last_name].join(' ')],
            documents: [{
                name: [this.user.first_name, this.user.middle_name, this.user.last_name].join(' '),
                email: this.user.email,
                phone_number: this.user.phone_number,
                day: Number.parseInt(this.kyc.day),
                month: Number.parseInt(this.kyc.month),
                year: Number.parseInt(this.kyc.year),
                address_street: this.kyc.address_street_1 + ' ' + this.kyc.address_street_2,
                address_city: this.kyc.address_city,
                address_country_code: this.kyc.address_country_code,
                address_postal_code: this.kyc.address_postal_code,
                address_subdivision: this.kyc.address_subdivision,
                entity_scope: this.kyc.entity_scope,
                entity_type: this.kyc.entity_type,
                virtual_docs: [{
                    document_type: 'SSN',
                    document_value: this.kyc.ssn
                }],
                physical_docs: [{
                    document_type: 'GOVT_ID',
                    document_value: this.kyc.govt_id
                }]
            }],
            extra: {
                supp_id: '',
                cip_tag: 1,
                is_business: false
            }
        }

        return payload
    }

}