
export namespace Synapse {
    export type storage = {
        id: string,
        saving: {
            id: string
            balance: string | number
            monthly_withdrawals_remaining: number,
            account_num: string,
            routing_num: {
                ach: string,
                wire: string
            }
        },
        nodes: Array<node>,
        permission: string

    }

    export type node = {
        id: string;
        nickname: string;
        balance: string | number
    }

}
