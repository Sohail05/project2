export namespace Synapse {
    export type docs = {
        document_type: string;
        document_value: string;
    }
}