import { Synapse } from './synapse-storage';

export namespace Onward {

    export type user = {
        email: string;
        first_name: string;
        last_name: string;
        middle_name: string;
        phone_number: string;
        frequency: string;
        employerId: string;
        employeeId: string;
        groupId: string;
        referralCode: string;
        sps?: Synapse.storage
    }

    export type Goal = {
        name: string
        createdAt: string
        default?: boolean,
        active: boolean,
        targetAmount: number,
        amount: number,
        targetDate: string,
        deduction: number,
        status?: string,
        cycles: number
    }

    export type Savings = {
        createdAt: string,
        goals: { [key: string]: Goal }
        employerId: string,
        referralCode: string,
        totalDeduction: number,
        cycles: number,
        id: string
        payroll: { [key: string]: Payroll }
    }

    export type Payroll = {
        amount: number
        date: string
        fees: number
    }

    export type customer = {
        tin: string
        name: string
        size: string
        industry: string
        stateCode: string
        referralCode: string
        groups: { [key: string]: group }
    }

    export type group = {
        code: string
        name: string
        frequency: string
        employees: { [key: string]: employee }
    }

    export type employee = {
        phone?: string
        email?: string
        employeeId: string
        referralCode?: string
    }

    export type referralCode = {
        active: boolean,
        used_at?: string,
        employerId: string,
        frequency: string,
        referralCode: string,
        created_at: string,
        employeeId: string,
        groupId: string,
    }

}