export interface SynapseApiUser {

    _id: string;
    _links: {self: {href:string} }
    client: {id:string, name:string}
    documents?: any;
    extra?:any;
    is_hidden: boolean;
    legal_names: Array<string>
    logins:Array<any>
    permission: string
    phone_numbers: Array<string>
    photos: Array<string>
    refresh_token: string

}