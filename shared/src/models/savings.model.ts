import { Onward } from '../types/types';

export class Savings implements Onward.Savings {

    id: string
    createdAt: string;
    goals: { [key: string]: Onward.Goal };
    employerId: string;
    referralCode: string;
    totalDeduction: number;
    cycles: number;
    uid: string
    payroll: { [key: string]: Onward.Payroll }

    constructor(properties: {}) {
        Object.assign(this, properties);
        console.log(this);
    }

    getTotalDeductions(): number {

        let sum = 0.00;
        for (let key in this.goals) {
            if (typeof this.goals[key].deduction === 'number') {
                sum = sum + this.goals[key].deduction;
            } else {
                sum = sum + Number.parseFloat(<any>this.goals[key].deduction);
            }
        }
        return sum
    }

    getGoals(): Array<Onward.Goal> {
        if (!this.goals) {
            return []
        }
        const keys = Object.keys(this.goals);
        return keys.map((key) => {
            return Object.assign({ gid: key, id: key }, this.goals[key])
        })
    }

}