import { Synapse } from '../types/synapse-storage';
export class SynapseInfo {
    
    id: string;
    saving: {
        id: string;
        balance: string | number;
    }
    subnet?: {
        id: string;
        routing_num: {
            ach: string;
            wire: string;
        }
    }
    nodes: Array<Synapse.node>
    permission: string;
    refresh_token: string;

    constructor(props?){
        Object.assign(this, props);
    }
}