export class Customer {
    constructor(properties?: {}) {
        Object.assign(this, properties);
    }
}