
import { Onward } from './../types/types'
export class GoalModel implements Onward.Goal {

    name;
    createdAt;
    default;
    active;
    targetAmount;
    amount;
    targetDate;
    deduction;
    status;
    cycles;

    constructor() { }
}