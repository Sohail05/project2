import { Onward } from '../types/types';
import { Synapse } from '../types/synapse-storage';

export class User implements Onward.user {

    id: string;
    email: string;
    phone_number: string;
    first_name: string;
    middle_name: string;
    last_name: string;
    frequency: string;
    employeeId: string;
    employerId: string;
    groupId: string;
    referralCode: string;
    sps?: Synapse.storage;

    constructor(properties: {}) {
        Object.assign(this, properties);
        console.log(this);
    }

}