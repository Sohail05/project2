import { SynapseLogin, SynapseUserDTO } from '../dto/synapse-user.dto';

export namespace callable {

    export namespace DTO {

        export interface CreateCustomer {
            name: string;
        }

        export interface GetSpsUser {
            uid: string;
            full_dehydrate: 'yes' | 'no'
        }

        export interface GetSpsUsers {
            query?: string;
            page?: number;
            per_page?: number;
            show_refresh_tokens?: 'yes' | 'no'
        }

        export interface CreateSpsUser {
            logins: Array<SynapseLogin>;
            phone_numbers: Array<string>;
            legal_names: Array<string>;
            documents?: Array<SynapseUserDTO>;
        }

        export interface UpdateSpsUser {
            uid: string;
        }

        export interface createAchNode {
            nickname: string;
            account_num: string;
            routing_num: string;
            oauth_key: string;
            uid: string;
        }

        export interface getAchNodes {
            uid: string;
            oauth_key: string;
        }

    }

    export namespace result {
        export interface AvailableFunds {
            amount: number;
        }
    }

}

