import { Injectable } from '@angular/core';
import { SynapseProvider } from '../synapse/synapse';

@Injectable()
export class WithdrawalProvider {

  formData = {
    reason: null,
    amount: null
  }
  achNode;

  constructor(private synapse: SynapseProvider) {
    console.log('Hello WithdrawalProvider Provider');
  }

  createTransaction(){
    return this.synapse.createWithdrawal({note: this.formData.reason, amount: this.formData.amount, dnid: this.achNode._id})
  }

}
