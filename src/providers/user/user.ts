import { Observable, } from 'rxjs/Observable';
import { switchMap, map } from 'rxjs/operators';
import { empty } from 'rxjs';
import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { UserDTO } from '../../../shared/src/dto/user.dto';
import { CallableProvider } from '../callable/callable';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../../shared/src';

@Injectable()
export class UserProvider {

  public currentUser: User;
  public obv: Observable<any>;

  constructor(
    private fb: AngularFireDatabase,
    private auth: AngularFireAuth,
    public callable: CallableProvider
  ) {

    this.currentUser = new User({});
    var user_id = '';

    this.obv = this.auth.user
      .pipe(map(user => user ? user.uid : null))
      .pipe(switchMap((uid) => {
        if (uid !== null && uid !== undefined) {
          user_id = uid;
          return this.fb.object(`/users/${uid}`).valueChanges()
        } else {
          return empty();
        }
      }))

    this.obv.subscribe((userData) => {
      if (userData) {
        this.currentUser = new User(Object.assign(userData, { id: user_id }));
      } else {
        this.currentUser = new User({});
      }
    })

  }

  /**
   * Add a user node.
   */
  create(user: UserDTO) {
    delete user['confirm_password'];
    delete user['password'];
    return this.callable.createUser(user);
  }

  /**
   * Get a user node by ID.
   */
  findById(id: string) {
    return this.fb.object(`/users/${id}`).valueChanges();
  }

  /**
   * Get a collection of users.
   */
  getAll() {
    return this.fb.list('/users/').valueChanges();
  }

  /**
   * Update user information.
   */
  update(id: string, body: Object) {
    return this.fb.object(`/users/${id}`).update(body);
  }

  /**
   * Remove user node by id.
   */
  remove(id: string) {
    return this.fb.object(`users/${id}`).remove();
  }


}
