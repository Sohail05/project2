
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { callable } from '../../../shared/src/callable';

@Injectable()
export class FlowProvider {

  constructor(private fb: AngularFireDatabase) { }

  /**
   * Create a new Synapse User & account.
   */
  create(user: callable.DTO.CreateSpsUser): Promise<any> {
    const fn = firebase.functions().httpsCallable('spsCreateAccount');
    return fn({ body: user });
  }

  /**
   * Validate Referral Code for Account Opening Flow.
   * @param referralCode
   * @returns 
   */
  validateReferralCode(rc: string) {
    return this.fb.database.ref().child('referralCodes').orderByChild('referralCode').equalTo(rc);
  }

  findAllReferralCodes() {
    return this.fb.database.ref().child('referralCodes').orderByChild('referralCode');
  }

  listCustomers() {
    return this.fb.database.ref('/customers')
  }

  listGroups(customerId) {
    return this.fb.database.ref('/customers/' + customerId + '/groups')
  }

  listEmployees(customerId, groupId) {
    return this.fb.database.ref('/customers' + customerId + '/employees/' + groupId)
  }

  tips() {
    return this.fb.database.ref('/tips')
  }

  savedTips(uid) {
    return this.fb.database.ref('saved/' + uid + '/tips');
  }

  notifications(uid) {
    return this.fb.database.ref('notifications/' + uid);
  }

  userMessages(uid) {
    return this.fb.database.ref('messages/users/' + uid);
  }

  groupMessages(uid) {
    return this.fb.database.ref('messages/groups/' + uid);
  }

  customerMessages(uid) {
    return this.fb.database.ref('messages/customers/' + uid);
  }

  insertCustomer() {
    return this.fb.database.ref('customers');
  }

  balance(user_id, access_token) {
    const fn = firebase.functions().httpsCallable('balance');
    return fn({ user_id, access_token })
  }

  availableFunds() {
    const fn = firebase.functions().httpsCallable('availableFunds');
    return fn()
  }

  TotalBalance() {
    const fn = firebase.functions().httpsCallable('balance');
    return fn();
  }

  getAchNodes() {
    const fn = firebase.functions().httpsCallable('getAchNodes');
    return fn();
  }

  getAccessToken() {
    const fn = firebase.functions().httpsCallable('getAccessToken');
    return fn();
  }

}
