import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { UserDTO } from '../../../shared/src';
import { EmployerRequestDTO } from '../../../shared/src/dto/employer-request.dto';
import { GoalDTO } from '../../../shared/src/dto/goal.dto';

@Injectable()
export class CallableProvider {

  functions: firebase.functions.Functions;

  constructor() {
    this.functions = firebase.functions();
  }

  referralCodeLookUp(data: string) {
    return this.functions.httpsCallable('referralCodeLookUp')(data)
  }

  employerRequest(data: EmployerRequestDTO) {
    return this.functions.httpsCallable('employerRequest')(data)
  }

  tips(data: { like?: number, tip: string, save?: boolean, remove?: boolean }) {
    return this.functions.httpsCallable('tips')(data)
  }

  accountClosureRequest(data: string) {
    return this.functions.httpsCallable('accountClosureRequest')(data);
  }

  verifyAch(data: any) {
    return this.functions.httpsCallable('verifyAch')(data)
  }

  withdrawalTransaction(data: { oauth_key, uid, snid, dnid, amount, note }) {
    return this.functions.httpsCallable('withdrawalTransaction')(data);
  }

  createUser(data: UserDTO) {
    return this.functions.httpsCallable('createUser')(data);
  }

  deleteAchNode(data: { nid, uid, oauth_key }) {
    return this.functions.httpsCallable('deleteAchNode')(data);
  }

  setSaving(data: { deduction, targetDate, targetAmount }) {
    const d = Object.assign(data, { name: 'Savings' });
    return this.functions.httpsCallable('createGoal')(d);
    //return this.functions.httpsCallable('setupEmergencyFunds')(data);
  }

  createGoal(data: { name, deduction, targetDate, targetAmount }) {
    return this.functions.httpsCallable('createGoal')(data);
  }

  removeGoal(data: { gid: string }) {
    return this.functions.httpsCallable('removeGoal')(data);
  }

  editGoal(data: GoalDTO) {
    return this.functions.httpsCallable('editGoal')(data);
  }

}
