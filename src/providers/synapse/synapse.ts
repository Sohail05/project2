import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { CallableProvider } from '../callable/callable';
import { SynapseAuthPostResultDTO } from '../../../shared/src/dto/synapse-oauth';
import { FlowProvider } from '../flow/flow';
import { UserProvider } from '../user/user';
import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class SynapseProvider {

  oauth: SynapseAuthPostResultDTO;
  uid;
  nid;

  constructor(
    public callable: CallableProvider,
    public flow: FlowProvider,
    public user: UserProvider,
    public auth: AngularFireAuth
  ) {
    this.oauth = JSON.parse(localStorage.getItem('synapse-token-' + this.uid))
  }

  /**
   * Prepare oauth for request.
   */
  prelaunch(): Promise<any> {

    if (this.user.currentUser.sps) {
      this.uid = this.user.currentUser['sps']['id'];
      this.nid = this.user.currentUser['sps']['saving']['id'];
    }

    // is token invalid && has token expired?
    if (!this.oauth || (new Date().getTime()) > Number.parseInt(this.oauth.expires_at + '000')) {
      return this.updateSession()
    }

    return Promise.resolve(true);
  }

  /**
   * Get new oauth token.
   */
  updateSession(): Promise<void> {
    return this.flow.getAccessToken().then((result) => {
      localStorage.setItem('synapse-token-' + this.uid, JSON.stringify(result.data));
      this.oauth = result.data;
    })
  }

  verifyAch(micro: Array<number>, nid): Promise<any> {
    return this.prelaunch().then(() => {
      return this.callable.verifyAch({ micro: micro, nid: nid, oauth_key: this.oauth.oauth_key, uid: this.uid })
    })
  }


  createWithdrawal(data: { note, amount, dnid }): Promise<any> {
    return this.prelaunch().then(() => {
      return this.callable.withdrawalTransaction({ note: data.note, dnid: data.dnid, amount: data.amount, snid: this.nid, oauth_key: this.oauth.oauth_key, uid: this.uid })
    })
  }

  createAch(data): Promise<any> {
    return this.prelaunch().then(() => {
      const fn = firebase.functions().httpsCallable('createAchNode');
      return fn(Object.assign(data, { uid: this.uid, oauth_key: this.oauth.oauth_key }));
    })
  }

  getAchNodes(): Promise<any> {
    return this.prelaunch().then(() => {
      const fn = firebase.functions().httpsCallable('getAchNodes');
      return fn({ uid: this.uid, oauth_key: this.oauth.oauth_key });
    })
  }

  deleteAchNode(nid): Promise<any> {
    return this.prelaunch().then(() => {
      return this.callable.deleteAchNode({ nid: nid, oauth_key: this.oauth.oauth_key, uid: this.uid })
    })
  }

  transactionHistory(page?: number): Promise<any> {
    return this.prelaunch().then(() => {
      const fn = firebase.functions().httpsCallable('transactionHistory');
      return fn({ uid: this.uid, oauth_key: this.oauth.oauth_key, page: page || 1, nid: this.nid });
    })
  }


}
