import { AngularFireDatabase } from 'angularfire2/database';
import { Injectable } from '@angular/core';
import { CallableProvider } from '../callable/callable';
import { AngularFireAuth } from 'angularfire2/auth';
import { Savings } from '../../../shared/src';
import { SavingsModel } from '../../pages/savings/savings.model';
import * as firebase from 'firebase';
@Injectable()
export class SavingsProvider {

  savings: Savings;
  obv;

  constructor(
    private fb: AngularFireDatabase,
    private auth: AngularFireAuth,
    public callable: CallableProvider,
    public db: AngularFireDatabase
  ) {

    this.savings = new Savings({});

    this.auth.user.subscribe((user) => {
      if (user) {
        this.obv = this.fb.object(`savings/${user.uid}`).valueChanges()
          .subscribe((savingData) => {
            if (savingData) {
              console.log('savings changes', savingData);
              this.savings = new Savings(Object.assign(savingData, { uid: user.uid }));
            }
          })
      } else {
        this.savings = new Savings({});
      }
    })
  }

  loadUserSavings(userId) {
    return this.db.database.ref().child('savings').orderByChild('userId').equalTo(userId);
  }

  loadCustomerSavings(employerId) {
    return this.db.list('savings').valueChanges();//.orderByChild('employerId')//.equalTo(employerId);
  }

  store(savings: SavingsModel, userData) {
    const fn = firebase.functions().httpsCallable('createSavings');
    return fn(savings);
  }

  paymentCycle(uid: string) {
    const fn = firebase.functions().httpsCallable('paymentCycle');
    return fn({ userId: uid });
  }

}
