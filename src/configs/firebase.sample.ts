export const firebaseConfig = {
    apiKey: "xxxxxxxxxx",
    authDomain: "your-domain-name.firebaseapp.com",
    databaseURL: "https://your-domain-name.firebaseio.com",
    storageBucket: "your-domain-name.appspot.com",
    messagingSenderId: '<your-messaging-sender-id>'
};