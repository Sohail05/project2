
export enum PageIndexes {
    Splash = "SplashPage",
    Login = "LoginPage",
    ForgotPassword = "ForgotPasswordPage",
    EmployerRequest = "EmployerRequestPage",
    SignUpGuide = "SignUpGuidePage",
    SignUp = "SignUpPage",
}