import { ErrorHandler } from '@angular/core';

export class ErrorHandlerLogger implements ErrorHandler {
    handleError(err: any): void {
        try {
            var req = new XMLHttpRequest();
            req.open("POST", "https://us-central1-onward-launch-5b8fb.cloudfunctions.net/errorHandler");
            console.error(err);
            req.send(JSON.stringify(err));
        } catch(e){
            console.error(e);
        }
    }
}