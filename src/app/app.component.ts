import { AngularFireAuth } from 'angularfire2/auth';
import { HomePage } from '../pages/home/home';
import { SplashPage } from '../pages/splash/splash';
import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  public rootPage: any = SplashPage;

  @ViewChild(Nav) nav: Nav;

  pages: any[] = [
    { title: 'Splash', component: 'SplashPage' },
    { title: 'SignUp Onward', component: 'SignUpPage' },
    { title: 'SignUp Synapse', component: 'SynapseSignUpPage' },
    { title: 'Tutorial', component: 'TutorialPage' },
    { title: 'Start Saving', component: 'StartSavingPage' },
    { title: 'Create Goal', component: 'CreateGoalPage' },
    { title: 'Home', component: 'HomePage' },
    { title: 'Dashboard', component: 'DashboardPage' },
    { title: 'MyAccount', component: 'AccountPage' },
    { title: 'Debug', component: 'DebugPage' },
    { title: 'SavingsSetPage', component: 'SavingsSetPage' },
    { title: 'Goal Detail Page', component: 'GoalDetailPage' },
    { title: 'Goal Deleted Page', component: 'GoalDeletedPage' },
    { title: 'AdminHome', component: 'AdminHomePage' },
    { title: 'Reset', component: 'ResetPasswordPage' },
    { title: 'Close', component: 'CloseAccountRequestPage' },
  ]

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private authProvider: AngularFireAuth,
    public menuCtrl: MenuController
  ) {

    const urlParams = new URLSearchParams(window.location.search);
    const dev = urlParams.get('dev');
    this.menuCtrl.enable( dev ? true :false)

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    }).then(() => {
      this.authentication();
    })
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  authentication() {
    this.authProvider.user.subscribe((user) => {
      if (user) {
        if (this.nav && this.nav.getActive() && this.nav.getActive().component && this.nav.getActive().component.name !== 'SignUpPage') {
          this.openPage({ component: HomePage });
        }
      } else {
        this.openPage({ component: SplashPage });
      }
    });
  }

}



