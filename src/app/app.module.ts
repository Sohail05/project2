import { HttpModule } from '@angular/http';
import { SplashPageModule } from '../pages/splash/splash.module';
import { ComponentsModule } from '../components/components.module';
import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

import { firebaseConfig } from '../configs/firebase';
import { firebaseConfig as firebaseDevConfig } from '../configs/firebase.dev';
import { UserProvider } from '../providers/user/user';
import { HttpClientModule } from '@angular/common/http';
import { FlowProvider } from '../providers/flow/flow';
import { TutorialPageModule } from '../pages/tutorial/tutorial.module';
import { SignUpPageModule } from '../pages/sign-up/sign-up.module';
import { HomePageModule } from '../pages/home/home.module';
import { SavingsPageModule } from '../pages/savings/savings.module';
import { SavingsProvider } from '../providers/savings/savings';
import { SynapseSignUpPageModule } from '../pages/synapse-sign-up/synapse-sign-up.module';
import { Camera } from '@ionic-native/camera';
import { WithdrawalPageModule } from '../pages/withdrawal/withdrawal.module';
import { BeneficiaryPageModule } from '../pages/beneficiary/beneficiary.module';
import { AddBeneficiaryPageModule } from '../pages/add-beneficiary/add-beneficiary.module';
import { SynapseProvider } from '../providers/synapse/synapse';
import { WithdrawalTransactionPageModule } from '../pages/withdrawal-transaction/withdrawal-transaction.module';
import { WithdrawalProvider } from '../providers/withdrawal/withdrawal';
import { VerifyBeneficiaryPageModule } from '../pages/verify-beneficiary/verify-beneficiary.module';
import { CallableProvider } from '../providers/callable/callable';
import { TransactionHistoryPageModule } from '../pages/transaction-history/transaction-history.module';
import { ClickStopPropagationDirective } from './directives/click-stop-propagation.directive';
import { FileValueAccessorDirective } from './directives/file-value-accessor.directive';
import { AdminHomePageModule } from '../pages/admin-home/admin-home.module';
import { AdminNewCustomerPageModule } from '../pages/admin-new-customer/admin-new-customer.module';

const urlParams = new URLSearchParams(window.location.search);
let dev = urlParams.get('dev') ? true : false;
dev = true;
@NgModule({
  declarations: [
    MyApp,
    ClickStopPropagationDirective,
    FileValueAccessorDirective,
  ],
  imports: [
    ComponentsModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(dev ? firebaseDevConfig : firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpClientModule,
    HttpModule,
    SplashPageModule,
    TutorialPageModule,
    SignUpPageModule,
    HomePageModule,
    SavingsPageModule,
    WithdrawalPageModule,
    SynapseSignUpPageModule,
    BeneficiaryPageModule,
    AddBeneficiaryPageModule,
    WithdrawalTransactionPageModule,
    VerifyBeneficiaryPageModule,
    TransactionHistoryPageModule,
    AdminHomePageModule,
    AdminNewCustomerPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    AngularFireDatabase,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UserProvider,
    FlowProvider,
    Camera,
    SynapseProvider,
    WithdrawalProvider,
    CallableProvider,
    SavingsProvider
  ],
  exports: [ClickStopPropagationDirective]
})
export class AppModule { }