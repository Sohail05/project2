import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { PageIndexes } from '../../pages-index';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PageSettings } from '../../page-settings';

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public auth: AngularFireAuth,
    public toastCtrl: ToastController
  ) { }

  email = '';

  goToSplash() {
    this.navCtrl.setRoot(PageIndexes.Splash);
  }

  /**
   * Submit Reset Form.
   */
  submitReset() {

    this.auth.auth.sendPasswordResetEmail(this.email)
      .then((request) => {
        this.processRequest();
        console.log(request);
      }).catch((error) => {
        if (error.code === "auth/invalid-email") {

          const toast = this.toastCtrl.create({
            message: error.message,
            duration: PageSettings.duration
          })
          toast.present()

        } else {
          console.log(error);
          this.errorRequest();
        }

      });
  }

  processRequest() {

    this.alertCtrl.create({
      title: 'Password Reset Submitted',
      message: `
        Your Password Reset email will be sent soon.
      `,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.goToSplash()
        }
      }]
    }).present();
  }

  errorRequest() {

    this.alertCtrl.create({
      title: 'An error has occured',
      message: `
        Please contact Onward if this error persists.
      `,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.goToSplash()
        }
      }]
    }).present();
  }

}
