import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-debug',
  templateUrl: 'debug.html'
})
export class DebugPage {

  api;
  data;
  response;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DebugPage');
  }

  exec() {
    const fn = firebase.functions().httpsCallable(this.api);
    const obj = JSON.parse(this.data);
    fn(obj).then((response) => {
      this.response = response.data;
    })
  }

}
