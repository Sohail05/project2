import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, TextInput, AlertController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../../../shared/src';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';


@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  userData: User;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public auth: AngularFireAuth,
    public alertCtrl: AlertController,
    public toast: ToastController
  ) { }

  ionViewWillEnter() {

    this.userData = this.userProvider.currentUser;

    if (!this.userData) {
      this.navCtrl.setRoot('HomePage');
    }

    console.log('ionViewDidLoad ProfilePage');
    console.log(this.userData);
  }

  goToCloseAccount() {
    this.navCtrl.push('CloseAccountRequestPage');
  }

  goToPasswordReset() {
    this.navCtrl.push('ResetPasswordPage')
  }

  formatPhone() {
    let frag = [];
    var phone: string = this.userData.phone_number
    phone = phone.replace(/[\W\s\._\-]+/g, '');
    frag[0] = phone.slice(0, 3);
    frag[1] = phone.slice(3, 6);
    frag[2] = phone.slice(6, 10);
    frag[0].concat("_".repeat(3 - frag[0].length))
    frag[1].concat("_".repeat(3 - frag[1].length))
    frag[1].concat("_".repeat(4 - frag[1].length))
    this.userData.phone_number =
      `(${frag[0]}` +
      (phone.length > 3 ? `) ${frag[1]}` : '') +
      (phone.length > 6 ? `-${frag[2]}` : '')

  }

  legal_name() {
    if (this.userData) {
      return `${this.userData.first_name} ${this.userData.middle_name} ${this.userData.last_name}`
    } else {
      return '';
    }
  }

  updateEmail(email, credentials: { email, password }) {

    console.log(email, credentials);

    this.auth.auth
      .signInWithEmailAndPassword(credentials.email, credentials.password)
      .then((user) => {
        user.user.updateEmail(email)
          .catch(() => {
            this.toast.create({
              message: "Unsupported email",
              duration: 6000
            }).present();
            return { error: true }
          })
          .then(
            (result) => {

              if (result && result['error']) {
                setTimeout(() => {
                  this.navCtrl.push('ProfilePage')
                }, 500)
                return;
              }
              console.log(result);
              this.userProvider.update(this.userData.id, { email })
              this.userData.email = email;
              setTimeout(() => {
                this.navCtrl.push('ProfilePage')
              }, 500)
            },
            (error) => {
              console.log(error);
            }
          )
      })
  }

  editEmail(event: TextInput) {
    console.log(event);
    if (event.value !== this.userData.email) {
      this.alertCtrl.create({
        title: 'Email Change',
        message: 'This operation is sensitive and requires authentication. Please enter your current email and password.',
        inputs: [{
          name: 'email',
          placeholder: 'Email',
          type: 'email'
        },
        {
          name: 'password',
          placeholder: 'Password',
          type: 'password'
        }
        ],
        buttons: [{
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }, {
          text: 'CONFIRM',
          handler: (data) => {
            this.updateEmail(event.value, { email: data.email, password: data.password })
          }
        }]
      }).present();

    }

  }

  editPhone(event) {
    if (event.value !== this.userData.phone_number) {
      this.alertCtrl.create({
        title: 'Phone # Change',
        message: 'This will change your account’s contact phone # to ' + event.value + '.',
        buttons: [{
          text: 'CANCEL',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }, {
          text: 'CONFIRM',
          handler: () => {
            this.userProvider.update(this.userData.id, { phone_number: event.value })
            this.userData.phone_number = event.value;
          }
        }]
      }).present();
    }
  }


}
