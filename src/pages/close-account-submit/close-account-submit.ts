import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-close-account-submit',
  templateUrl: 'close-account-submit.html',
})
export class CloseAccountSubmitPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CloseAccountSubmitPage');
  }

  goToHomePage() {
    this.navCtrl.setRoot('HomePage');
  }

}
