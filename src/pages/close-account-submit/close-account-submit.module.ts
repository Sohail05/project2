import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CloseAccountSubmitPage } from './close-account-submit';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CloseAccountSubmitPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CloseAccountSubmitPage),
  ],
})
export class CloseAccountSubmitPageModule {}
