import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { SavingsProvider } from '../../providers/savings/savings';
import { CallableProvider } from '../../providers/callable/callable';

@IonicPage()
@Component({
  selector: 'page-goal-detail',
  templateUrl: 'goal-detail.html',
})
export class GoalDetailPage {


  gid: string;
  //goalData: Onward.Goal = {name:name}
  goalData = { name: 'My Goal' }

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public savings: SavingsProvider,
    public alertCtrl: AlertController,
    public savingsProvider: SavingsProvider,
    public callable: CallableProvider,
    public loadingCtrl: LoadingController
  ) { }

  ionViewDidLoad() {

    if (typeof this.navParams.data === 'string') {
      this.loadGoal()
    } else {
      this.goalData = this.navParams.data;
    }

    console.log('Goal Detail:', this.goalData);
  }

  loadGoal() {
    if (this.savingsProvider.savings && this.savingsProvider.savings.goals) {
      this.goalData = this.savingsProvider.savings.goals[this.navParams.data];
    }
  }

  editGoal() {
    this.navCtrl.push('GoalEditPage', this.navParams.data);
  }

  deleteGoal() {
    this.alertCtrl.create({
      title: 'Goal deleting confirmation',
      message: 'Are you sure you want to delete your "' + this.goalData.name + '" Goal?',
      buttons: [{
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Yes',
        handler: () => {
          this.performDelete();
        }
      }]
    }).present();

  }

  performDelete() {

    const loading = this.loadingCtrl.create();
    loading.present();
    this.callable.removeGoal({ gid: this.navParams.data }).then(() => {
      this.navCtrl.push('GoalDeletedPage', this.goalData.name);
    },
      (error) => {
        console.log(error);
      }
    ).then(() => {
      loading.dismiss();
    })

  }


}
