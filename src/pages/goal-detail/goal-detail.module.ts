import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoalDetailPage } from './goal-detail';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GoalDetailPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(GoalDetailPage),
  ],
})
export class GoalDetailPageModule { }
