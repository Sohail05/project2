import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { VerifyBeneficiaryPage } from './verify-beneficiary';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    VerifyBeneficiaryPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(VerifyBeneficiaryPage),
  ],
})
export class VerifyBeneficiaryPageModule {}
