import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { WithdrawalProvider } from '../../providers/withdrawal/withdrawal';

@IonicPage()
@Component({
  selector: 'page-verify-beneficiary',
  templateUrl: 'verify-beneficiary.html',
})
export class VerifyBeneficiaryPage {

  node
  returnPage;
  micro = [0.1, 0.1]

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public synapse: SynapseProvider,
    public loadingCtrl: LoadingController,
    public withdrawal: WithdrawalProvider
  ) {

    this.node = this.navParams.get('node');
    this.returnPage = this.navParams.get('page');

  }

  ionViewDidLoad() {
    console.log('104000029');
    console.log('ionViewDidLoad VerifyBeneficiaryPage');
  }

  addMicro() {
    this.micro.push(0.1);
  }

  removeMicro(index) {
    delete this.micro[index];
  }

  verify() {

    const loading = this.loadingCtrl.create();
    loading.present();
    if (this.node['_id']) {
      this.synapse.verifyAch(this.micro, this.node['_id']).then((result) => {
        console.log(result);
        this.withdrawal.achNode = this.node
        this.navCtrl.push(this.returnPage || 'AccountPage');
      }, (error) => {
        console.log(error);
      }).then(() => {
        loading.dismiss();
      })
    }
  }

}
