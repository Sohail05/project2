import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FlowProvider } from '../../providers/flow/flow';
import { UserProvider } from '../../providers/user/user';


@IonicPage()
@Component({
  selector: 'page-messages',
  templateUrl: 'messages.html',
})
export class MessagesPage {


  messages = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public flow: FlowProvider,
    public user: UserProvider,
  ) { }

  ionViewDidLoad() {
    this.flow.userMessages(this.user.currentUser.id).on("value", (snapshot) => {
      this.messages = snapshot.val();
      console.log(this.messages);
      
    })
  }

  getKeys(object) {
    if (!object) {
      return [];
    }

    return Object.keys(object)
  }

}
