import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { SynapseSignUpPage } from '../synapse-sign-up/synapse-sign-up';
import { User } from '../../../shared/src/models/user.model';
import { Onward } from '../../../shared/src/types/types';

@IonicPage()
@Component({
  selector: 'page-sign-up',
  templateUrl: 'sign-up.html',
})
export class SignUpPage {

  code: Onward.referralCode;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public authProvider: AngularFireAuth,
    public toast: ToastController,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingController
  ) { }

  ionViewDidLoad() {
    const key = Object.keys(this.navParams.data).shift();
    this.code = this.navParams.data[key];
  }

  userRegistration(payload: UserFormModel) {

    const loading = this.loadingCtrl.create();
    loading.present();


    this.authProvider.auth.createUserWithEmailAndPassword(payload.email, payload.password).then(
      () => {
        const user = new User(Object.assign(payload, this.code));
        this.userProvider.create(user).then((data) => {
          console.log(data);
          this.toSynapseSignUp();
        }, (error) => {
          console.log(error);
        });
      },
      (error) => {
        console.log(error);
        this.notify('Sign Up Failed.' + ' Error: ' + error.message);
      }
    ).then(() => loading.dismiss());

  }

  toSynapseSignUp() {
    this.navCtrl.setRoot(SynapseSignUpPage);
  }

  notify(message) {
    this.toast.create({ message: message, duration: 3000 }).present();
  }

}

