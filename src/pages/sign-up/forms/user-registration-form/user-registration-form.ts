import { ToastController } from 'ionic-angular';
import { Component, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'user-registration-form',
  templateUrl: 'user-registration-form.html'
})
export class UserRegistrationFormComponent {

  @Output() onSubmit = new EventEmitter<string>();
  user: FormGroup;

  get legal_name() {
    return [
      this.user.get('first_name').value,
      this.user.get('middle_name').value,
      this.user.get('last_name').value
    ].join(' ');
  }

  constructor(private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.user = this.formBuilder.group({
      first_name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z \-]+')])],
      middle_name: [''],
      last_name: ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z \-]+')])],
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9_]).{8,64})')])],
      confirm_password: ['', Validators.compose([Validators.required, Validators.pattern('((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9_]).{8,64})')])],
      phone_number: ['', Validators.compose([Validators.required, Validators.minLength(14)])],
    }, { updateOn: 'submit' })
  }

  registerForm() {

    this.validateAllFormFields(this.user);
    this.user.markAsDirty();
    this.user.updateValueAndValidity();


    if (this.user.value['password'] !== this.user.value['confirm_password']) {
      this.toastCtrl.create({ message: 'Password and Confirm Password must match.', duration: 3000 }).present();
      return;
    }

    if (this.user.valid) {
      this.onSubmit.emit(Object.assign(this.user.value, { legal_name: this.legal_name }));
    } else {
      this.toastCtrl.create({ message: 'Validation failed.', duration: 3000 }).present();
    }
  }

  formatPhone() {
    let frag = [];
    var phone: string = this.user.value['phone_number'];
    phone = phone.replace(/[\W\s\._\-]+/g, '');
    frag[0] = phone.slice(0, 3);
    frag[1] = phone.slice(3, 6);
    frag[2] = phone.slice(6, 10);
    frag[0].concat("_".repeat(3 - frag[0].length))
    frag[1].concat("_".repeat(3 - frag[1].length))
    frag[1].concat("_".repeat(4 - frag[1].length))
    this.user.controls['phone_number'].setValue(
      `(${frag[0]}` +
      (phone.length > 3 ? `) ${frag[1]}` : '') +
      (phone.length > 6 ? `-${frag[2]}` : '')
    )
  }

  match(value: string, regex: string): boolean {
    return value.match(new RegExp(regex)) ? true : false
  }


  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
        control.updateValueAndValidity()
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}


