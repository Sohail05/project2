type SpsFormModel = {
    address_street_1: string;
    address_street_2: string;
    address_city: string;
    address_subdivision: string;
    address_postal_code: string;
    address_country_code: string;
    dob: string;
    entity_type: string;
    entity_scope: string;
    ssn: string;
    govt_id: string;
};

type UserFormModel = {
    first_name: string,
    middle_name: string,
    last_name: string,
    email: string,
    password: string,
    phone: string,
    legal_name: string
};