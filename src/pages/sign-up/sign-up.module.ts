import { UserRegistrationFormComponent } from './forms/user-registration-form/user-registration-form';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignUpPage } from './sign-up';
import { ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SignUpPage,
    UserRegistrationFormComponent
  ],
  exports: [],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SignUpPage),
    ReactiveFormsModule,
  ],

})
export class SignUpPageModule { }
