import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-reset-password',
  templateUrl: 'reset-password.html',
})
export class ResetPasswordPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public auth: AngularFireAuth,
    public user: UserProvider,
  ) {
  }

  email = '';

  ionViewDidLoad() {

    this.email = this.user.currentUser.email;
    console.log('ionViewDidLoad ResetPasswordPage');
  }

  goToProfile() {
    this.navCtrl.push('ProfilePage');
  }

  // Todo reset password
  submitReset() {

    this.auth.auth.sendPasswordResetEmail(this.email)
      .then((request) => {
        this.processRequest();
        console.log(request);
      }).catch((error) => {
        console.log(error);
        this.errorRequest()
      });
  }

  processRequest() {
    this.alertCtrl.create({
      title: 'Password Reset Submitted',
      message: `
        Your Password Reset email will be sent soon.
      `,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.push('ProfilePage');
        }
      }]
    }).present();
  }

  errorRequest() {
    this.alertCtrl.create({
      title: 'An error has occured',
      message: `
        Please contact Onward if this error persists.
      `,
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.push('ProfilePage');
        }
      }]
    }).present();
  }

}
