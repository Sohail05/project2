import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-select-saving',
  templateUrl: 'select-saving.html',
})
export class SelectSavingPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectSavingPage');
  }

  goToStartSaving() {
    this.navCtrl.push('StartSavingPage');
  }

  goToCreateGoal() {
    this.navCtrl.push('CreateGoalPage');
  }

}
