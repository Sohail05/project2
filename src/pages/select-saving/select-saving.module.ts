import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SelectSavingPage } from './select-saving';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SelectSavingPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SelectSavingPage),
  ],
})
export class SelectSavingPageModule {}
