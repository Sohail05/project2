import { Component, EventEmitter, Output, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular';


@Component({
  selector: 'withdrawal-form',
  templateUrl: 'withdrawal-form.html'
})
export class WithdrawalFormComponent {

  withdrawal: FormGroup;
  @Input() max: number;
  @Output() onSubmit = new EventEmitter<{ amount, reason }>();

  constructor(private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.withdrawal = this.formBuilder.group({
      amount: [0, Validators.compose([Validators.required, Validators.max(this.max), Validators.min(0)])],
      reason: ['', Validators.required],
    }, { updateOn: 'submit' });
  }

  submitForm() {

    this.validateAllFormFields(this.withdrawal);
    this.withdrawal.markAsDirty();
    this.withdrawal.updateValueAndValidity();

    if (this.withdrawal.value['amount'] > this.max) {
      const toast = this.toastCtrl.create({ message: `Amount cannot be greater than the available funds of ${this.max}.`, duration: 6000 });
      toast.present();
      return;
    }

    if (this.withdrawal.value['amount'] <= 0) {
      const toast = this.toastCtrl.create({ message: `Amount must be greater than 0.`, duration: 6000 });
      toast.present();
      return;
    }


    if (this.withdrawal.valid) {
      this.onSubmit.emit(this.withdrawal.value);
    } else {
      const toast = this.toastCtrl.create({ message: "Validation failed.", duration: 6000 });
      toast.present();
    }
  }

  formatCurrency() {
    const amount = Number.parseFloat(this.withdrawal.value['amount']);
    if (amount && typeof amount === "number") {
      this.withdrawal.controls['amount']
        .setValue(amount.toFixed(2));
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
        control.updateValueAndValidity()
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
