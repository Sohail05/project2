import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WithdrawalPage } from './withdrawal';
import { WithdrawalFormComponent } from './forms/withdrawal-form/withdrawal-form';
import { ComponentsModule } from '../../components/components.module';
WithdrawalFormComponent
@NgModule({
  declarations: [
    WithdrawalPage,
    WithdrawalFormComponent,
  ],
  imports: [
    IonicPageModule.forChild(WithdrawalPage),
    ComponentsModule
  ],
})
export class WithdrawalPageModule {}
