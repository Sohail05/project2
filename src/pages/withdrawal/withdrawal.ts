import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { FlowProvider } from '../../providers/flow/flow';
import { callable } from '../../../shared/src/callable';
import { BeneficiaryPage } from '../beneficiary/beneficiary';
import { WithdrawalProvider } from '../../providers/withdrawal/withdrawal';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-withdrawal',
  templateUrl: 'withdrawal.html',
})
export class WithdrawalPage {

  funds = null;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alert: AlertController,
    public flow: FlowProvider,
    public withdrawalProvider: WithdrawalProvider,
    public loadingCtrl: LoadingController,
    public userProvider: UserProvider,
  ) { }

  ionViewDidLoad() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.flow.availableFunds().then(
      (result) => {
        this.evaluate(result.data);
      },
      (error) => {
        console.log(error);
      }
    ).then(() => {
      loading.dismiss();
    })

  }

  submitWithdrawal(data) {
    this.withdrawalProvider.formData = data;
    this.navCtrl.push(BeneficiaryPage);
  }

  goToHome() {
    this.navCtrl.setRoot('HomePage');
  }

  redirectHome() {
    this.alert.create({
      title: 'Unavailable Funds',
      message: 'No Free Funds Available for Withdrawal',
      buttons: [{
        text: 'Home',
        handler: () => {
          this.goToHome();
        }
      }]
    }).present();
  }

  evaluate(availableFunds: callable.result.AvailableFunds) {

    this.funds = availableFunds.amount;
    if (availableFunds.amount > 0) {
    } else {
      this.redirectHome();
    }

  }

  continue(data) {
    this.alert.create({
      title: 'Withdraw money',
      message: 'Withdraw money only for emergencies to make your hard work count.',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Ok',
        handler: () => {
          this.submitWithdrawal(data);
        }
      }]
    }).present();
  }


}
