import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { WithdrawalProvider } from '../../providers/withdrawal/withdrawal';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { SavingsProvider } from '../../providers/savings/savings';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public synapse: SynapseProvider,
    public withdrawalProvider: WithdrawalProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public savingsProvider: SavingsProvider,
    public userProvider: UserProvider
  ) {
  }

  nodes = [];
  savingNode;

  ionViewDidLoad() {

    this.userProvider.obv.subscribe(() => {
      if (this.userProvider.currentUser && this.userProvider.currentUser.sps) {
        this.savingNode = this.userProvider.currentUser.sps;
      }
    }, err => {
      console.log(err);
    }, () => {
      console.log("completed");
    })

    const loading = this.loadingCtrl.create();
    loading.present();
    this.synapse.getAchNodes().then((result) => {
      console.log(result);
      this.nodes = result.data['nodes'];
    }, (error) => {
      console.log(error);
    }).then(() => {
      loading.dismiss();
    })
  }

  moveToAddBeneficiary() {
    this.navCtrl.push('AddBeneficiaryPage');
  }

  goToVerify(node) {
    this.navCtrl.push('VerifyBeneficiaryPage', { node, page: 'MicroDepositDetailPage' });
  }

  deleteNode(node) {
    this.alertCtrl.create({
      title: 'Connected Account deleting confirmation',
      message: 'Are you sure you want to delete this connected account?',
      buttons: [{
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Yes',
        handler: () => {
          this.performDelete(node);
        }
      }]
    }).present();
  }

  performDelete(node) {

    const loading = this.loadingCtrl.create();
    loading.present();
    this.synapse.deleteAchNode(node['_id']).then(
      (success) => {
        console.log(success);
        this.navCtrl.push('DeleteBeneficiaryPage', 'MyACH');
      },
      (error) => {
        console.log(error);

      },
    ).then(() => {
      loading.dismiss();
    })
  }

}
