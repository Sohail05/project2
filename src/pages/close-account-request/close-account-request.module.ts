import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CloseAccountRequestPage } from './close-account-request';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CloseAccountRequestPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CloseAccountRequestPage),
  ],
})
export class CloseAccountRequestPageModule {}
