import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CallableProvider } from '../../providers/callable/callable';

@IonicPage()
@Component({
  selector: 'page-close-account-request',
  templateUrl: 'close-account-request.html',
})
export class CloseAccountRequestPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public callable: CallableProvider
  ) {
  }

  reason = '';

  ionViewDidLoad() {
    console.log('ionViewDidLoad CloseAccountRequestPage');
  }

  goToProfile() {
    this.navCtrl.push('ProfilePage');
  }

  /**
   * Confirm Submit Alert.
   */
  closeRequest() {
    this.alertCtrl.create({
      title: 'Account closing confirmation',
      message: `
      Are you sure you want to leave Onward? \n
      Don’t miss out on all the benefits of a healthier financial life
      `,
      buttons: [{
        text: 'No',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Yes',
        handler: () => {
          this.performClosure();
        }
      }]
    }).present();
  }

  /**
   * Loading & Call request.
   */
  performClosure() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.callable.accountClosureRequest(this.reason).then(
      (result) => {
        this.displayMessage()
        console.log(result)
      },
      (error) => {
        console.log(error);
      }
    ).then(() => loading.dismiss());
  }


  displayMessage() {
    this.alertCtrl.create({
      title: 'Thank you.',
      message: 'Your request has been received.',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.navCtrl.push('CloseAccountSubmitPage');
        }
      }]
    }).present();
  }

}
