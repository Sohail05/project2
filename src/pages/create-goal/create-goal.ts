import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-create-goal',
  templateUrl: 'create-goal.html',
})
export class CreateGoalPage {
  @ViewChild('content') content: any;

  list = [
    { title: 'REPAIR A CAR', img: 'repair.svg' },
    { title: 'PAY OFF CREDIT CARD DEBTS', img: 'credit.svg' },
    { title: 'EMERGENCY FUND', img: 'emergency.svg' },
    { title: 'SAVING FOR CHILDREN', img: 'family.svg' },
    { title: 'SAVING FOR RETIREMENT', img: 'retire.svg' },
    { title: 'SAVING FOR COLLEGE', img: 'school.svg' },
    { title: 'SAVING FOR MARRIAGE', img: 'marriage.svg' },
    { title: 'BUY A CAR', img: 'car.svg' },
    { title: 'BUY A HOUSE', img: 'family.svg' },
    { title: 'Set Custom Goal', img: '', showInput: true },
  ]

  selected;
  name = "";
  displayInput = false;
  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateGoalPage');
  }

  goalSelect(item) {

    if (item['showInput']) {
      this.displayInput = true;
      setTimeout(() => {
        this.content.scrollToBottom(300);
      }, 50);
    } else {
      this.navCtrl.push('CreateGoalCalculationPage', { name: item.title });
    }
  }

  goToCalculation() {
    this.navCtrl.push('CreateGoalCalculationPage', { name: this.name });
  }

}
