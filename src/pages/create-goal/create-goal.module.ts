import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateGoalPage } from './create-goal';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CreateGoalPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CreateGoalPage),
  ],
})
export class CreateGoalPageModule {}
