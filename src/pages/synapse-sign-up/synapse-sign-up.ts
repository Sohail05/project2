import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { TutorialPage } from '../tutorial/tutorial';
import { UserProvider } from '../../providers/user/user';
import { AngularFireAuth } from 'angularfire2/auth';
import { UserDTO } from '../../../shared/src/dto/user.dto';
import { User } from '../../../shared/src/models/user.model';
import { UserParser } from '../../../shared/src/user-parser';
import { FlowProvider } from '../../providers/flow/flow';

@IonicPage()
@Component({
  selector: 'page-synapse-sign-up',
  templateUrl: 'synapse-sign-up.html',
})
export class SynapseSignUpPage {

  public base64Image: string;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public userProvider: UserProvider,
    public toast: ToastController,
    public loadingCtrl: LoadingController,
    public authProvider: AngularFireAuth,
    public flowProvider: FlowProvider,
  ) { }

  userData: UserDTO;
  id;
  tos: false;

  ionViewDidLoad() {

    console.log('1600 Pennsylvania Ave NW, Washington, DC 20500, USA');
    this.authProvider.user.subscribe((authUser) => {
      if (!authUser) {
        return;
      }
      this.id = authUser.uid;
      this.userProvider.findById(authUser.uid).subscribe((userData: UserDTO) => {
        this.userData = userData;
      })
    })
  }

  spsRegistration(payload: SpsFormModel) {

    if (!this.tos) {
      const toast = this.toast.create({ message: 'Must agree to Onwards Terms of Service & Privacy Policy.', duration: 6000 });
      toast.present();
      return
    }


    const loading = this.loadingCtrl.create();
    loading.present();

    const spsUser = new UserParser(payload, new User(this.userData)).SynapseUserCreateDTO();

    this.flowProvider.create(spsUser).then(
      (result) => {

        if (result.data.error) {

          this.notify(result.data.error.en);
          console.log(result.data.error);

        } else {

          localStorage.setItem('access_token', result.data.oauth.oauth_key);
          this.toTutorial();

        }
      }, (error) => {
        this.notify('Synapse Error');
        console.log(error);
      }).then(() => loading.dismiss());

  }

  toTutorial() {
    this.navCtrl.setRoot(TutorialPage);
  }

  notify(message) {
    this.toast.create({ message: message, duration: 6000 }).present();
  }

  tosClick(){
    window.open("https://getonward.org/terms-of-service/",'_system', 'location=yes');
  }
  ppClick(){
    window.open("https://getonward.org/private-policy/",'_system', 'location=yes');
  }

}


