import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SynapseSignUpPage } from './synapse-sign-up';
import { SpsRegistrationFormComponent } from './forms/sps-registration-form/sps-registration-form';
import { ComponentsModule } from '../../components/components.module';
@NgModule({
  declarations: [
    SynapseSignUpPage,
    SpsRegistrationFormComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SynapseSignUpPage),
  ],
})
export class SynapseSignUpPageModule { }
