import { Component, Output, EventEmitter } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Platform } from 'ionic-angular';
@Component({
  selector: 'sps-registration-form',
  templateUrl: 'sps-registration-form.html'
})
export class SpsRegistrationFormComponent {

  @Output() onSubmit = new EventEmitter<SpsFormModel>();

  base64Image;

  sps: FormGroup;
  base64image: string;
  entityTypes = [
    { text: 'Male', value: 'M' },
    { text: 'Female', value: 'F' },
    { text: 'Do Not Wish to Specify', value: 'NOT_KNOWN' },
  ]

  entityScopes = [
    'Automotive',
    'Bank & Financial Services',
    'Education',
    'Energy/Utility',
    'Engineering/Construction',
    'Event Planning/Event Services',
    'Food/Grocery',
    'Government Organization',
    'Home Improvement',
    'Hospital/Clinic',
    'Hotel',
    'School',
    'Shopping/Retail',
    'Telecommunication',
    'Transport/Freight',
  ];

  states = [{ code: "AL", text: "Alabama" },
  { code: "AK", text: "Alaska" },
  { code: "AZ", text: "Arizona" },
  { code: "AR", text: "Arkansas" },
  { code: "CA", text: "California" },
  { code: "CO", text: "Colorado" },
  { code: "CT", text: "Connecticut" },
  { code: "DE", text: "Delaware" },
  { code: "DC", text: "District of Columbia" },
  { code: "FL", text: "Florida" },
  { code: "GA", text: "Georgia" },
  { code: "HI", text: "Hawaii" },
  { code: "ID", text: "Idaho" },
  { code: "IL", text: "Illinois" },
  { code: "IN", text: "Indiana" },
  { code: "IA", text: "Iowa" },
  { code: "KS", text: "Kansas" },
  { code: "KY", text: "Kentucky" },
  { code: "LA", text: "Louisiana" },
  { code: "ME", text: "Maine" },
  { code: "MD", text: "Maryland" },
  { code: "MA", text: "Massachusetts" },
  { code: "MI", text: "Michigan" },
  { code: "MN", text: "Minnesota" },
  { code: "MS", text: "Mississippi" },
  { code: "MO", text: "Missouri" },
  { code: "MT", text: "Montana" },
  { code: "NE", text: "Nebraska" },
  { code: "NV", text: "Nevada" },
  { code: "NH", text: "New Hampshire" },
  { code: "NJ", text: "New Jersey" },
  { code: "NM", text: "New Mexico" },
  { code: "NY", text: "New York" },
  { code: "NC", text: "North Carolina" },
  { code: "ND", text: "North Dakota" },
  { code: "OH", text: "Ohio" },
  { code: "OK", text: "Oklahoma" },
  { code: "OR", text: "Oregon" },
  { code: "PA", text: "Pennsylvania" },
  { code: "RI", text: "Rhode Island" },
  { code: "SC", text: "South Carolina" },
  { code: "SD", text: "South Dakota" },
  { code: "TN", text: "Tennessee" },
  { code: "TX", text: "Texas" },
  { code: "UT", text: "Utah" },
  { code: "VT", text: "Vermont" },
  { code: "VA", text: "Virginia" },
  { code: "WA", text: "Washington" },
  { code: "WV", text: "West Virginia" },
  { code: "WI", text: "Wisconsin" },
  { code: "WY", text: "Wyoming" }];

  //{ code: "AS", text: "American Samoa" },
  //{ code: "GU", text: "Guam" },
  //{ code: "MP", text: "Northern Mariana Islands" },
  //{ code: "PR", text: "Puerto Rico" },
  //{ code: "UM", text: "United States Minor Outlying Islands" },
  //{ code: "VI", text: "Virgin Islands, U.S." }]

  get ssn() {
    return this.sps.value['ssn'].replace(/[\W\s\._\-]+/g, '');
  }

  constructor(
    private formBuilder: FormBuilder,
    private toastCtrl: ToastController,
    private camera: Camera,
    public plt: Platform
  ) {

    console.log(this.plt.platforms());
    this.sps = this.formBuilder.group({
      address_street_1: ['', Validators.required],
      address_street_2: [''],
      address_city: ['', Validators.compose([Validators.required, Validators.pattern('[^0-9]*')])],
      address_subdivision: ['', Validators.required],
      address_postal_code: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.pattern('[0-9]*')])],
      address_country_code: ['US', Validators.required],
      day: ['', Validators.required],
      month: ['', Validators.required],
      year: ['', Validators.required],
      entity_type: ['', Validators.required],
      entity_scope: ['Not Known', Validators.required],
      ssn: ['', Validators.compose([Validators.required, Validators.pattern('[0-9]{3}-[0-9]{2}-[0-9]{4}')])],
      govt_id: ['', Validators.required]
    }, { updateOn: 'submit' });
  }

  registerForm() {

    this.validateAllFormFields(this.sps);
    this.sps.markAsDirty();
    this.sps.updateValueAndValidity();

    const dob = [this.sps.value['day'], this.sps.value['month'], this.sps.value['year']].join('/');
    console.log(dob);
    console.log(new Date(dob));

    if (18 > new Date().getFullYear() - new Date(dob).getFullYear()) {
      const toast = this.toastCtrl.create({ message: 'Must be older than 18', duration: 6000 });
      toast.present();
      return
    }

    if (this.sps.valid) {
      this.onSubmit.emit(Object.assign(this.sps.value, { image: this.base64image }));
    } else {
      this.getFormValidationErrors();
      const toast = this.toastCtrl.create({ message: 'Validation failed.', duration: 6000 });
      toast.present();
    }
  }

  getFormValidationErrors() {
    let errors = '';
    Object.keys(this.sps.controls).forEach(key => {
      const controlErrors = this.sps.get(key).errors;

      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          errors += 'Key control: ' + key + ', keyError: ' + keyError + '\n';
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });

    return errors;
  }

  formatSSN() {
    let frag = [];
    var ssn: string = this.sps.value['ssn'];
    ssn = ssn.replace(/[\W\s\._\-]+/g, '');
    frag[0] = ssn.slice(0, 3);
    frag[1] = ssn.slice(3, 5);
    frag[2] = ssn.slice(5, 9);
    frag[0].concat("_".repeat(3 - frag[0].length))
    frag[1].concat("_".repeat(2 - frag[1].length))
    frag[1].concat("_".repeat(4 - frag[1].length))
    this.sps.controls['ssn'].setValue(
      `${frag[0]}` +
      (ssn.length > 3 ? `-${frag[1]}` : '') +
      (ssn.length > 5 ? `-${frag[2]}` : '')
    )

  }

  /**
   * Read files from event.
   * @param event 
   */
  imageFileHandler(event: Event) {
    let files = event.target['files'];
    let file = files[0];

    if (files && file) {
      let reader = new FileReader();
      reader.onload = (readerEvent) => {
        this.sps.get('govt_id').setValue('data:' + file.type + ';base64,' + btoa(readerEvent.target['result']));
      }
      reader.readAsBinaryString(file);
    }
  }

  options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }

  takePicture() {

    this.camera.getPicture(this.options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.sps.get('govt_id').setValue(base64Image);
    }, (err) => {
      // Handle error
    });
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
        control.updateValueAndValidity()
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  normalize(event: KeyboardEvent) {
    if (event.target['max'] && event.target['value'] && (event.target['value'] > event.target['max'])) {
      event.target['value'] = event.target['max']
    }

    if (event.target['min'] && event.target['value'] && (event.target['value'] < event.target['min'])) {
      event.target['value'] = event.target['min']
    }

  }

  maxYear() {
    return new Date().getFullYear() - 17;
  }

}