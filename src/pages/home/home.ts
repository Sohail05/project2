import { SavingsProvider } from '../../providers/savings/savings';
import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';
import { FlowProvider } from '../../providers/flow/flow';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  tips;
  savedTips;

  constructor(
    public navigation: NavController,
    public userProvider: UserProvider,
    public savingsProvider: SavingsProvider,
    public flow: FlowProvider,
    public afap: AngularFireAuth,
  ) { }

  ionViewWillEnter() {

    this.afap.user.subscribe((user) => {
      if (user) {
        this.userProvider.obv.subscribe((next) => {
          if (next && !next['sps'])
            this.navigation.push('SynapseSignUpPage');
        })
      }
    })

    this.flow.savedTips(this.userProvider.currentUser.id).on('value', (savedSnapshot) => {
      this.savedTips = savedSnapshot.val();
      console.log(this.savedTips);
    })

    this.flow.tips().once('value', (snap) => {
      this.tips = snap.val();
      console.log(this.tips);
    })


  }

  getKeys(object) {
    if (!object) {
      return [];
    }

    return Object.keys(object)
  }

  getDeductions() {
    return this.savingsProvider.savings.getTotalDeductions();
  }

  getSavingsBalance() {
    return this.userProvider.currentUser.sps.saving.balance;
  }

  goToTutorial() {
    this.navigation.push('TutorialPage');
  }

  goToHistory() {
    this.navigation.push('TransactionHistoryPage');
  }

  goToCreateGoal() {
    this.navigation.push('CreateGoalPage');
  }

  goToSaving() {
    //this.navigation.push('SelectSavingPage');
    this.navigation.push('StartSavingPage');
  }

  goToSavings() {
    this.navigation.push('SavingsPage');
  }

  goToGoal(val) {
    this.navigation.push('GoalDetailPage', val);
  }

}