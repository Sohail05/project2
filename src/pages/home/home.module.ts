import { HomePage } from './home';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
    declarations: [
        HomePage,
    ],
    exports: [],
    imports: [
        ComponentsModule,
        IonicPageModule.forChild(HomePage),
    ],

})
export class HomePageModule { }
