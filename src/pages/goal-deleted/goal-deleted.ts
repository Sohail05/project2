import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-goal-deleted',
  templateUrl: 'goal-deleted.html',
})
export class GoalDeletedPage {

  name = '';
  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoalDeletedPage');
    this.name = this.navParams.get('name')
  }

  goToHome() {
    this.navCtrl.setRoot('HomePage');
  }

}
