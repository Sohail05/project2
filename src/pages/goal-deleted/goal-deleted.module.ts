import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoalDeletedPage } from './goal-deleted';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GoalDeletedPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(GoalDeletedPage),
  ],
})
export class GoalDeletedPageModule {}
