import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminNewGroupPage } from './admin-new-group';

@NgModule({
  declarations: [
    AdminNewGroupPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminNewGroupPage),
  ],
})
export class AdminNewGroupPageModule {}
