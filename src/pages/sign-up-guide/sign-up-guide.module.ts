import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SignUpGuidePage } from './sign-up-guide';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SignUpGuidePage,
  ],
  imports: [
    IonicPageModule.forChild(SignUpGuidePage),
    ComponentsModule
  ],
})
export class SignUpGuidePageModule { }
