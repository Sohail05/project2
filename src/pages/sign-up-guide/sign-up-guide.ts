import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PageIndexes } from '../../pages-index';

@IonicPage()
@Component({
  selector: 'page-sign-up-guide',
  templateUrl: 'sign-up-guide.html',
})
export class SignUpGuidePage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignUpGuidePage');
  }

  goToSignUp() {
    this.navCtrl.setRoot(PageIndexes.SignUp, this.navParams.data);
  }

}
