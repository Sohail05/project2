import { SavingsGoalFormComponent } from './forms/savings-goal-form/savings-goal-form';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavingsPage } from './savings';

@NgModule({
  declarations: [
    SavingsPage,
    SavingsGoalFormComponent
  ],
  imports: [
    IonicPageModule.forChild(SavingsPage),
  ],
})
export class SavingsPageModule {}
