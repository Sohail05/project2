import * as moment from 'moment';

export class SavingsModel {

    name: string;
    amount: number = 10.00;
    targetDate: string = moment(moment.now()).add(1, 'M').add(1, 'd').format('YYYY-MM-DD');
    deduction: number = 1.00;
    frequency: 'weekly' | 'bi-weekly' | 'monthly' = 'monthly';
    userId: string;
    employerId: string;
    status: string;
    confirmation: 'pending' | 'approved' | 'refused';

    get minDate(): string {
        return moment(moment.now()).add(1, 'M').add(1, 'd').format('YYYY-MM-DD');
    }

    get months(): number {
        return Math.ceil(moment(this.targetDate).diff(moment(moment.now()), 'months'))
    }

    get weeks(): number {
        return Math.ceil(moment(this.targetDate).diff(moment(moment.now()), 'weeks'))
    }

    get isValid(): boolean {
        if (this.targetDate && this.deduction && this.amount) {
            return true;
        }
        return false;
    }

    constructor(properties?: {}) {
        Object.assign(this, properties);
    }

    calculateTargetDate(): void {
        this.targetDate = moment(moment.now()).add(this.amount / this.deduction, 'M').format('YYYY-MM-DD');
    }

    calculateDeduction(): void {

        this.deduction = parseFloat((this.amount / this.frequencyUnit()).toFixed(2));
    }

    calculateSavingGoal(): void {
        this.amount = parseFloat((this.frequencyUnit() * this.deduction).toFixed(2));
    }

    frequencyUnit(): number {

        let units = 0;
        switch (this.frequency) {
            case 'weekly':
                units = this.weeks;
                break;
            case 'bi-weekly':
                units = Math.ceil(this.weeks / 2)
                break;
            default:
                units = this.months;
        }

        return units
    }

    setMonths(months){
        this.targetDate = moment(moment.now()).add(months, 'M').format('YYYY-MM-DD');
    }

    setWeeks(weeks){
        this.targetDate = moment(moment.now()).add(weeks, 'w').format('YYYY-MM-DD');
    }

}