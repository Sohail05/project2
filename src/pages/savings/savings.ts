import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides, AlertController, LoadingController } from 'ionic-angular';
import { SavingsModel } from './savings.model'
import { SavingsProvider } from '../../providers/savings/savings';
import { UserProvider } from '../../providers/user/user';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-savings',
  templateUrl: 'savings.html',
})
export class SavingsPage {

  @ViewChild(Slides) slides: Slides;
  public savings: SavingsModel;

  // @debug
  result;
  userData;

  constructor(
    public navigation: NavController,
    public alert: AlertController,
    public navParams: NavParams,
    public savingsProvider: SavingsProvider,
    public authProvider: AngularFireAuth,
    public userProvider: UserProvider,
    public loadingCtrl: LoadingController,
  ) {
    this.savings = new SavingsModel()
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SavingsPage');
    this.slides.lockSwipes(true);

    this.authProvider.user.subscribe((authUser) => {
      if (!authUser) {
        return;
      }
      this.userProvider.findById(authUser.uid).subscribe((userData) => {
        this.userData = userData;
      })
    })

  }

  nextStep() {
    this.slides.lockSwipes(false);
    this.slides.slideNext(500);
    this.slides.lockSwipes(true);
  }

  confirm() {
    this.presentConfirm()
  }

  processDeduction() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.savingsProvider.store(this.savings, this.userData).then(
      (data) => {
        this.result = data;
      },
      (error) => {
        console.log(error);
      }
    ).then(() => loading.dismiss());
    this.nextStep();
  }

  back() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev(500);
    this.slides.lockSwipes(true);
  }

  goHome() {
    this.navigation.setRoot('HomePage');
  }

  presentConfirm() {
    let alert = this.alert.create({
      title: 'Monthly deduction',
      message: 'Confirm monthly deduction of ' + this.savings.deduction + ' ?',
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      },
      {
        text: 'Confirm',
        handler: () => {
          this.processDeduction();
        }
      }
      ]
    });
    alert.present();
  }




}
