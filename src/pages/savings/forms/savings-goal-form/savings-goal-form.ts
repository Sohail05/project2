import { Component, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'savings-goal-form',
  templateUrl: 'savings-goal-form.html'
})
export class SavingsGoalFormComponent {

  @Output() onSubmit = new EventEmitter<any>();

  form : FormGroup

  constructor(private formBuilder: FormBuilder) {
    this.form = this.formBuilder.group({
      value: [''],
      period: [''],
    }, { updateOn: 'submit' });
  }

  submitForm(){
    this.onSubmit.emit(this.form.value);
  }

}
