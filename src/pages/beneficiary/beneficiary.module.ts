import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeneficiaryPage } from './beneficiary';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    BeneficiaryPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(BeneficiaryPage),
  ],
})
export class BeneficiaryPageModule {}
