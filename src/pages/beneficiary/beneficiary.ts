import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { AddBeneficiaryPage } from '../add-beneficiary/add-beneficiary';
import { SynapseProvider } from '../../providers/synapse/synapse';
import { WithdrawalTransactionPage } from '../withdrawal-transaction/withdrawal-transaction';
import { VerifyBeneficiaryPage } from '../verify-beneficiary/verify-beneficiary';
import { WithdrawalProvider } from '../../providers/withdrawal/withdrawal';

@IonicPage()
@Component({
  selector: 'page-beneficiary',
  templateUrl: 'beneficiary.html',
})
export class BeneficiaryPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public synapse: SynapseProvider,
    public withdrawalProvider: WithdrawalProvider,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController
  ) {
  }

  nodes = [];

  ionViewDidLoad() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.synapse.getAchNodes().then((result) => {
      console.log(result);
      this.nodes = result.data['nodes'];
    }, (error) => {
      console.log(error);
    }).then(() => {
      loading.dismiss();
    })
  }

  moveToAddBeneficiary() {
    this.navCtrl.push(AddBeneficiaryPage, { page: 'BeneficiaryPage' });
  }

  selectNode(node) {

    this.alertCtrl.create({
      title: 'Confirm withdrawal',
      message: 'Confirm the withdrawal of $' + this.withdrawalProvider.formData.amount + '?',
      buttons: [{
        text: 'CANCEL',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'CONFIRM',
        handler: () => {
          this.withdrawalProvider.achNode = node;
          this.performTransaction()
        }
      }]
    }).present();

  }

  performTransaction() {

    const loading = this.loadingCtrl.create();
    loading.present();
    this.withdrawalProvider.createTransaction().then((result) => {
      this.navCtrl.push(WithdrawalTransactionPage);
    }, (error) => {
      console.log(error);
    }).then(() => {
      loading.dismiss();
    })


  }

  goToVerify(node) {
    this.navCtrl.push(VerifyBeneficiaryPage, { node, page: 'BeneficiaryPage' });
  }


}
