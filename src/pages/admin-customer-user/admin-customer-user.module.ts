import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminCustomerUserPage } from './admin-customer-user';

@NgModule({
  declarations: [
    AdminCustomerUserPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminCustomerUserPage),
  ],
})
export class AdminCustomerUserPageModule {}
