import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminNewCustomerPage } from './admin-new-customer';
import { ComponentsModule } from '../../components/components.module';
import { AdminNewCustomerFormComponent } from './form/admin-new-customer-form/admin-new-customer-form';

@NgModule({
  declarations: [
    AdminNewCustomerPage,
    AdminNewCustomerFormComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AdminNewCustomerPage),
  ],
})
export class AdminNewCustomerPageModule {}
