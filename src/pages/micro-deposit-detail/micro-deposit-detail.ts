import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-micro-deposit-detail',
  templateUrl: 'micro-deposit-detail.html',
})
export class MicroDepositDetailPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MicroDepositDetailPage');
  }

  goToAccount() {
    this.navCtrl.push("AccountPage");
  }

}
