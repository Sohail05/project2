import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MicroDepositDetailPage } from './micro-deposit-detail';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MicroDepositDetailPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(MicroDepositDetailPage),
  ],
})
export class MicroDepositDetailPageModule {}
