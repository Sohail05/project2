import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { WithdrawalProvider } from '../../providers/withdrawal/withdrawal';
import { FlowProvider } from '../../providers/flow/flow';
import { HomePage } from '../home/home';


@IonicPage()
@Component({
  selector: 'page-withdrawal-transaction',
  templateUrl: 'withdrawal-transaction.html',
})
export class WithdrawalTransactionPage {

  data: {
    reason,
    amount,
    node
  };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public withdrawal: WithdrawalProvider,
    public flow: FlowProvider,
    public alert: AlertController,
    public loadingCtrl: LoadingController
  ) { }

  ionViewDidLoad() {
    this.data = {
      reason: this.withdrawal.formData.reason,
      amount: this.withdrawal.formData.amount,
      node: this.withdrawal.achNode
    }
  }

  confirmTransaction() {

    this.alert.create({
      title: 'Confirm Withdrawal',
      message: 'Transfer of ' + this.data.amount + '$ to ' + this.data.node.info.nickname,
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Confirm',
        handler: () => {
          this.performTransaction();
        }
      }]
    }).present();
  }

  performTransaction() {

    const loading = this.loadingCtrl.create();
    loading.present();
    this.withdrawal.createTransaction().then((result) => {
      console.log(result);
      this.navCtrl.setRoot(HomePage);
    }, (error) => {
      console.log(error);
    }).then(() => {
      loading.dismiss();
    })
  }

  goToHomePage() {
    this.navCtrl.setRoot('HomePage');
  }

}
