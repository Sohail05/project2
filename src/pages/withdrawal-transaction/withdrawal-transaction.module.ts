import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WithdrawalTransactionPage } from './withdrawal-transaction';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    WithdrawalTransactionPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(WithdrawalTransactionPage),
  ],
})
export class WithdrawalTransactionPageModule {}
