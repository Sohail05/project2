import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CallableProvider } from '../../providers/callable/callable';
import { PageIndexes } from '../../pages-index';

@IonicPage()
@Component({
  selector: 'page-employer-request',
  templateUrl: 'employer-request.html',
})
export class EmployerRequestPage {

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alert: AlertController,
    public nav: NavController,
    public callable: CallableProvider,
    public loadingCtrl: LoadingController
  ) { }

  /**
   * Form Submission
   * @param payload
   */
  submitRequest(payload) {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.callable.employerRequest(payload).then(
      (result) => { this.displayMessage() },
      (error) => {
        console.log(error);
      }
    )
      .then(() => loading.dismiss());
  }

  /**
   * Post Submission Message
   */
  displayMessage() {
    this.alert.create({
      title: 'Thank you.',
      message: 'Your request has been received.',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.nav.push(PageIndexes.Splash);
        }
      }]
    }).present();
  }

  goBack(){
    this.navCtrl.pop();
  }

}
