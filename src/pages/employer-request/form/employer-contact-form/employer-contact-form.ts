import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PageSettings } from '../../../../page-settings';

@Component({
  selector: 'employer-contact-form',
  templateUrl: './employer-contact-form.html'
})
export class EmployerContactFormComponent {

  request: FormGroup;
  @Output() onSubmit = new EventEmitter<string>();

  constructor(private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.request = this.formBuilder.group({
      company: ['', Validators.compose([Validators.required])],
      name: ['', Validators.compose([Validators.required])],
      location: ['', Validators.compose([Validators.required])],
      phone: [''],
      email: [''],
    }, { updateOn: 'submit' })
  }

  submitForm() {
    this.validateAllFormFields(this.request);
    this.request.markAsDirty();
    this.request.updateValueAndValidity();

    if (this.request.valid) {
      this.onSubmit.emit(this.request.value);
    } else {
      this.toastCtrl.create({ message: 'Please fill all required fields.', duration: PageSettings.duration }).present();
    }
  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
        control.updateValueAndValidity()
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  formatPhone() {
    let frag = [];
    var phone: string = this.request.value['phone'];
    phone = phone.replace(/[\W\s\._\-]+/g, '');
    frag[0] = phone.slice(0, 3);
    frag[1] = phone.slice(3, 6);
    frag[2] = phone.slice(6, 10);
    frag[0].concat("_".repeat(3 - frag[0].length))
    frag[1].concat("_".repeat(3 - frag[1].length))
    frag[1].concat("_".repeat(4 - frag[1].length))
    this.request.controls['phone'].setValue(
      `(${frag[0]}` +
      (phone.length > 3 ? `) ${frag[1]}` : '') +
      (phone.length > 6 ? `-${frag[2]}` : '')
    )
  }


}
