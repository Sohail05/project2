import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EmployerRequestPage } from './employer-request';
import { ComponentsModule } from '../../components/components.module';
import { EmployerContactFormComponent } from './form/employer-contact-form/employer-contact-form';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    EmployerRequestPage,
    EmployerContactFormComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(EmployerRequestPage),
    ReactiveFormsModule
  ],
})
export class EmployerRequestPageModule {}
