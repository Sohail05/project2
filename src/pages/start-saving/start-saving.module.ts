import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartSavingPage } from './start-saving';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    StartSavingPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(StartSavingPage),
  ],
})
export class StartSavingPageModule {}
