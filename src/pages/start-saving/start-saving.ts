import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { CallableProvider } from '../../providers/callable/callable';
import { SavingsModel } from '../savings/savings.model';

@IonicPage()
@Component({
  selector: 'page-start-saving',
  templateUrl: 'start-saving.html',
})
export class StartSavingPage {

  selectMode: 'auto' | 'manual' = 'auto';
  amount;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alert: AlertController,
    public callable: CallableProvider,
    public loadingCtrl: LoadingController
  ) { }

  formatCurrency() {
    const amount = Number.parseFloat(this.amount);
    if (amount && typeof amount === "number") {
      this.amount = amount.toFixed(2);
    }
  }

  submit() {

    const amount = Number.parseFloat(this.amount);
    if (!amount || amount <= 0) {
      return;
    }

    this.alert.create({
      title: 'Savings Deduction',
      message: `Confirm the saving deduction of $${this.amount} per paycheck?`,
      buttons: [{
        text: 'CANCEL',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Confirm',
        handler: () => {
          this.submitSaving();
        }
      }]
    }).present();
  }

  selectAmount(amount: number) {
    this.amount = amount.toFixed(2);
    this.submit();
  }

  submitSaving() {


    const savingM = new SavingsModel();
    savingM.setMonths(12);
    

    console.log('sending $' + this.amount + ' to ONB');
    const loading = this.loadingCtrl.create();
    loading.present();

    this.callable.setSaving({
      deduction: parseFloat(this.amount),
      targetDate: savingM.targetDate,
      targetAmount: Number.parseFloat(this.amount) * 12,
    }).then((result: { data: { key } }) => {
      console.log(result.data);
      this.navCtrl.push('SavingsSetPage', { gid: result.data.key });
    }, (error) => {
      console.log(error);
    }).then(() => loading.dismiss());

  }

}
