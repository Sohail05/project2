import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SavingsProvider } from '../../providers/savings/savings';

@IonicPage()
@Component({
  selector: 'page-savings-set',
  templateUrl: 'savings-set.html',
})
export class SavingsSetPage {

  goalData = {};
  gid;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public savingsProvider: SavingsProvider
  ) {
  }

  ionViewDidLoad() {
    this.gid = this.navParams.get('gid');
    console.log(this.navParams.data);
    this.loadGoal(this.gid)
  }

  loadGoal(gid) {
    if (this.savingsProvider.savings && this.savingsProvider.savings.goals) {
      this.goalData = this.savingsProvider.savings.goals[gid];
    } else {
      console.log('wait 1 sec');
      setTimeout(this.loadGoal, 1000);
    }
  }

  goToHomePage() {
    this.navCtrl.setRoot('HomePage');
  }

}
