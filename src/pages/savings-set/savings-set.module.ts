import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavingsSetPage } from './savings-set';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    SavingsSetPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(SavingsSetPage),
  ],
})
export class SavingsSetPageModule {}
