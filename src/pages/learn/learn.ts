import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FlowProvider } from '../../providers/flow/flow';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-learn',
  templateUrl: 'learn.html',
})
export class LearnPage {

  tips;
  savedTips;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public flow: FlowProvider,
    public userProvider: UserProvider
  ) { }

  ionViewDidLoad() {

    this.flow.savedTips(this.userProvider.currentUser.id).on('value', (savedSnapshot) => {
      this.savedTips = savedSnapshot.val();
      console.log(this.savedTips);
    })

    this.flow.tips().on('value', (snap) => {
      this.tips = snap.val();
      console.log(this.tips);
    })

  }

  getKeys(object) {
    if (!object) {
      return [];
    }
    return Object.keys(object)
  }

}
