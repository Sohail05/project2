import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { GoalModel } from '../../../shared/src/models/goal.model';
import { SavingsModel } from '../savings/savings.model';
import { CallableProvider } from '../../providers/callable/callable';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-create-goal-calculation',
  templateUrl: 'create-goal-calculation.html',
})
export class CreateGoalCalculationPage {

  list = [
    { amount: 10 },
    { amount: 20 },
    { amount: 30 },
    { amount: 40 },
  ]

  frequency = "bi-weekly";
  name = 'My Goal';

  selectMode: 'manual' | 'auto' = 'auto';
  public savings: SavingsModel;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public callable: CallableProvider,
    public userProvider: UserProvider
  ) {
    console.log('Params:', this.navParams.data);
    this.name = this.navParams.get('name') || 'My Goal';
    this.savings = new SavingsModel()
    this.frequency = this.userProvider.currentUser.frequency || 'weekly';
    this.savings.frequency = this.frequency as 'bi-weekly';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateGoalCalculationPage');
  }

  selectedDeduction(item) {

    switch (this.frequency) {
      case 'bi-weekly':
        this.savings.setWeeks(12 * 2);
        break
      case 'weekly':
        this.savings.setWeeks(12);
        break
      case 'monthly':
        this.savings.setMonths(12);
        break
      default:
        this.savings.setWeeks(12 * 2);
        break
    }

    this.savings.deduction = item.amount;
    this.savings.calculateSavingGoal();
    this.confirmation();
  }

  selectedOther() {
    this.selectMode = 'manual';
  }

  //To Modal
  confirmation() {
    this.alertCtrl.create({
      title: 'Paycheck deduction',
      message: `Confirm the deduction of $${this.savings.deduction} per paycheck?`,
      buttons: [{
        text: 'CANCEL',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'CONFIRM',
        handler: () => {
          this.createGoal();
        }
      }]
    }).present();
  }

  createGoal() {
    const goal = new GoalModel();
    goal.name = this.name;
    goal.amount = 0;
    goal.deduction = this.savings.deduction;
    goal.targetDate = this.savings.targetDate;
    goal.targetAmount = this.savings.amount;

    this.callable.createGoal(goal).then(
      (result: { data: { key: string } }) => {
        this.navCtrl.push('CreateGoalCompletedPage', goal);
      }, (error) => {
        console.log(error);
      })

  }

  //Todo
  /*processDeduction() {
    const loading = this.loadingCtrl.create();
    loading.present();
    this.savingsProvider.store(this.savings, this.userData).then(
      (data) => {
        this.result = data;
      },
      (error) => {
        console.log(error);
      }
    ).then(() => loading.dismiss());
    this.nextStep();
  }*/


}
