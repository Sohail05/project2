import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateGoalCalculationPage } from './create-goal-calculation';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CreateGoalCalculationPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CreateGoalCalculationPage),
  ],
})
export class CreateGoalCalculationPageModule { }
