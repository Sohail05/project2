import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminCustomerGroupPage } from './admin-customer-group';

@NgModule({
  declarations: [
    AdminCustomerGroupPage,
  ],
  imports: [
    IonicPageModule.forChild(AdminCustomerGroupPage),
  ],
})
export class AdminCustomerGroupPageModule {}
