import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AdminCustomerPage } from './admin-customer';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AdminCustomerPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AdminCustomerPage),
  ],
})
export class AdminCustomerPageModule {}
