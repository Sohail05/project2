import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-admin-customer',
  templateUrl: 'admin-customer.html',
})
export class AdminCustomerPage {

  groups

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
    console.log(this.navParams);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdminCustomerPage');
  }

  goToGroup(group) {
    this.navCtrl.push('AdminCustomerGroup', group)
  }

  goToNewGroup() {
    this.navCtrl.push('AdminNewGroup', this.navParams)
  }

}
