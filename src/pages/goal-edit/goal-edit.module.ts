import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GoalEditPage } from './goal-edit';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    GoalEditPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(GoalEditPage),
  ],
})
export class GoalEditPageModule {}
