import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { SavingsModel } from '../savings/savings.model';
import { SavingsProvider } from '../../providers/savings/savings';
import { CallableProvider } from '../../providers/callable/callable';

@IonicPage()
@Component({
  selector: 'page-goal-edit',
  templateUrl: 'goal-edit.html',
})
export class GoalEditPage {


  name = '';
  public savings: SavingsModel;
  frequency = "bi-weekly";
  goalData;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public savingsProvider: SavingsProvider,
    public callable: CallableProvider
  ) {
    this.savings = new SavingsModel()
    this.savings.frequency = this.frequency as 'bi-weekly';
    console.log(this.navParams.data);
    this.loadGoal();
  }

  loadGoal() {
    if (this.savingsProvider.savings && this.savingsProvider.savings.goals) {
      this.goalData = this.savingsProvider.savings.goals[this.navParams.data];
      this.savings.amount = this.goalData.targetAmount;
      this.savings.targetDate = this.goalData.targetDate;
      this.savings.name = this.goalData.name;
      this.savings.deduction = this.goalData.deduction;
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoalEditPage');
  }

  confirmation() {
    this.alertCtrl.create({
      title: 'Goal Edit',
      message: `Confirm changes to your Goal?`,
      buttons: [{
        text: 'CANCEL',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'CONFIRM',
        handler: () => {
          this.updateGoal();
        }
      }]
    }).present();
  }

  goToDetail() {
    this.navCtrl.pop();
  }

  updateGoal() {

    this.callable.editGoal({
      gid: this.navParams.data,
      name: this.savings.name,
      targetAmount: this.savings.amount,
      targetDate: this.savings.targetDate,
      deduction: this.savings.deduction
    }).then(
      (result: { data: { key: string } }) => {
        this.navCtrl.push('GoalDetailPage', this.navParams.data);
      }, (error) => {
        console.log(error);
      })
  }

}
