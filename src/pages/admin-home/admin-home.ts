import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FlowProvider } from '../../providers/flow/flow';

@IonicPage()
@Component({
  selector: 'page-admin-home',
  templateUrl: 'admin-home.html',
})
export class AdminHomePage {

  customers;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public flow: FlowProvider
  ) { }

  ionViewDidLoad() {
    this.flow.listCustomers().on('value', (snapshot) => {
      this.customers = snapshot.val()
      console.log(this.customers)
    });
  }

  goToCustomer(customer) {
    this.navCtrl.push('AdminCustomerPage', customer);
  }

  goToNewCustomer(){
    this.navCtrl.push('AdminNewCustomerPage');
  }

}
