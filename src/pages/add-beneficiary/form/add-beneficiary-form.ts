import { Component, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ToastController } from 'ionic-angular';

@Component({
  selector: 'add-beneficiary-form',
  templateUrl: './add-beneficiary-form.html',
})
export class AddBeneficiaryFormComponent {


  @Output() onSubmit = new EventEmitter<string>();
  beneficiary: FormGroup;

  nickname;
  account_num;
  routing_num;

  constructor(private formBuilder: FormBuilder, private toastCtrl: ToastController) {
    this.beneficiary = this.formBuilder.group({
      nickname: ['', Validators.compose([Validators.required])],
      account_num: ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(17)])],
      routing_num: ['', Validators.compose([Validators.required, Validators.minLength(9)])],
    }, { updateOn: 'submit' })
  }

  addbeneficiary() {
    this.validateAllFormFields(this.beneficiary);
    this.beneficiary.markAsDirty();
    this.beneficiary.updateValueAndValidity();

    if (!this.validRoutingNumber(this.beneficiary.value['routing_num'])) {
      const toast = this.toastCtrl.create({ message: 'Routing No. is not a valid ABA routing transit number.', duration: 6000 });
      toast.present();
      return
    }

    if (this.beneficiary.valid) {
      this.onSubmit.emit(this.beneficiary.value);
    } else {
      this.toastCtrl.create({ message: 'Validation failed.', duration: 6000 }).present();
    }
  }

  validRoutingNumber(routing) {
    if (routing.length !== 9) {
      return false;
    }

    const checksumTotal =
      (7 * (parseInt(routing.charAt(0), 10) + parseInt(routing.charAt(3), 10) + parseInt(routing.charAt(6), 10))) +
      (3 * (parseInt(routing.charAt(1), 10) + parseInt(routing.charAt(4), 10) + parseInt(routing.charAt(7), 10))) +
      (9 * (parseInt(routing.charAt(2), 10) + parseInt(routing.charAt(5), 10) + parseInt(routing.charAt(8), 10)));

    const checksumMod = checksumTotal % 10;
    if (checksumMod !== 0) {
      return false;
    } else {
      return true;
    }
  };

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
        control.updateValueAndValidity()
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

}
