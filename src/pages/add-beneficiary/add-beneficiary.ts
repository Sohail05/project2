import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SynapseProvider } from '../../providers/synapse/synapse';

@IonicPage()
@Component({
  selector: 'page-add-beneficiary',
  templateUrl: 'add-beneficiary.html',
})
export class AddBeneficiaryPage {

  error;
  returnPage;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public synapse: SynapseProvider,
    public loadingCtrl: LoadingController
  ) {

    this.returnPage = this.navParams.get('page');
    console.log('104000029');
  }

  back() {
    this.navCtrl.pop();
  }

  createAch(event) {
    this.error = null;
    const loading = this.loadingCtrl.create();
    loading.present();
    this.synapse
      .createAch(event)
      .then((result) => {
        if (result.data['error']) {
          this.error = result.data['error']['en'];
        } else {
          this.navCtrl.push(this.returnPage || 'MicroDepositDetailPage');
        }
        console.log(result);
      }, (error) => {
        console.log(error);
      }).then(() => {
        loading.dismiss();
      })
  }

}
