import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddBeneficiaryPage } from './add-beneficiary';
import { AddBeneficiaryFormComponent } from './form/add-beneficiary-form';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AddBeneficiaryPage,
    AddBeneficiaryFormComponent
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(AddBeneficiaryPage),
  ],
})
export class AddBeneficiaryPageModule {}
