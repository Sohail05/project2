import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { SynapseProvider } from '../../providers/synapse/synapse';

@IonicPage()
@Component({
  selector: 'page-transaction-history',
  templateUrl: 'transaction-history.html',
})
export class TransactionHistoryPage {

  loading;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public synapse: SynapseProvider,
    public loadingCtrl: LoadingController
  ) {
    this.loading = this.loadingCtrl.create();
  }

  transactions = [];

  //Todo: pagination for over 20 transactions.

  ionViewDidLoad() {
    this.loading.present();
    this.synapse.transactionHistory().then(
      (result) => {
        console.log(result);
        
        if (result.data['trans']) {
          this.transactions = result.data.trans;
        }
      },
      (error) => { 
        console.log(error)
      })
      .then(() => {
        this.loading.dismiss();
      });
  }

  ionViewWillLeave() {
    this.loading.dismiss();
  }



}
