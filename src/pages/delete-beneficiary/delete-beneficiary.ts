import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-delete-beneficiary',
  templateUrl: 'delete-beneficiary.html',
})
export class DeleteBeneficiaryPage {

  name = '';
  constructor(public navCtrl: NavController, public navParams: NavParams) { }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GoalDeletedPage');
    this.name = this.navParams.get('name')
  }

  goToHome() {
    this.navCtrl.setRoot('HomePage');
  }

}
