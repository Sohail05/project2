import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeleteBeneficiaryPage } from './delete-beneficiary';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    DeleteBeneficiaryPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(DeleteBeneficiaryPage),
  ],
})
export class DeleteBeneficiaryPageModule {}
