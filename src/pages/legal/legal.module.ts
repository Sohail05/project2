import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LegalPage } from './legal';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    LegalPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(LegalPage),
  ],
})
export class LegalPageModule {}
