import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-legal',
  templateUrl: 'legal.html',
})
export class LegalPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  link(url) {
    window.open(url, '_system', 'location=yes');
  }

  goToHome(url) {
    this.navCtrl.setRoot('HomePage')
  }

}


