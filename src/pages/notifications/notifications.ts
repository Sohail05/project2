import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FlowProvider } from '../../providers/flow/flow';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-notifications',
  templateUrl: 'notifications.html',
})
export class NotificationsPage {

  notifications = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public flow: FlowProvider,
    public user: UserProvider,
  ) { }

  ionViewDidLoad() {
    console.log("notifications");
    this.flow.notifications(this.user.currentUser.id).on("value", (snapshot) => {
      this.notifications = snapshot.val();
    })
  }

  getKeys(object) {
    if (!object) {
      return [];
    }

    return Object.keys(object)
  }

}
