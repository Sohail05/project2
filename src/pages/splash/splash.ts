import { Component } from '@angular/core';
import { IonicPage, NavController, ModalController, Modal } from 'ionic-angular';
import { AccessComponent } from '../../components/access/access';
import { PageIndexes } from '../../pages-index';

@IonicPage()
@Component({
  selector: 'page-splash',
  templateUrl: 'splash.html',
})
export class SplashPage {

  public modal: Modal;

  constructor(
    public navigation: NavController,
    public modalCtrl: ModalController
  ) { }

  goToLogin() {
    this.navigation.push("LoginPage");
  }

  signupModal() {
    this.modal = this.modalCtrl.create(AccessComponent, {}, { enableBackdropDismiss: true, showBackdrop: true });
    this.modal.present().then(result => {
      const access = this.modal.overlay['instance'] as AccessComponent;
      access.matched.subscribe((customer) => {
        this.modal.dismiss();
        console.log(customer)
        this.NavigateToSignUp(customer);
      })
    });
  }

  /**
   * Navigate to Sign Up Page with Customer data.
   * @param customer
   */
  private NavigateToSignUp(customer) {
    this.navigation.setRoot(PageIndexes.SignUpGuide, customer);
  }


}
