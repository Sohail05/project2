import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(
    public navigation: NavController,
    public authProvider: AngularFireAuth,
  ) { }

  signOut() {
    this.authProvider.auth.signOut().then(() => {
      this.navigation.push('SplashPage')
    })
  }

  goToSaving() {
    //this.navigation.push('SelectSavingPage');
    this.navigation.push('StartSavingPage');
  }

  goToGoal() {
    this.navigation.push('CreateGoalPage');
  }

  goToWithdrawal() {
    this.navigation.push('WithdrawalPage');
  }

  goToAccount() {
    this.navigation.push('AccountPage');
  }

  goToProfile() {
    this.navigation.push('ProfilePage');
  }

  goToHelp() {
    this.navigation.push('HelpPage');
  }

  goToLearn() {
    this.navigation.push('LearnPage');
  }

  goToLegal() {
    this.navigation.push('LegalPage');
  }

}
