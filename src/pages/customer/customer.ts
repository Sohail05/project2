import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SavingsProvider } from '../../providers/savings/savings';
import { SavingsModel } from '../savings/savings.model';
import { FlowProvider } from '../../providers/flow/flow';

@IonicPage()
@Component({
  selector: 'page-customer',
  templateUrl: 'customer.html',
})
export class CustomerPage {

  phone;
  email;
  savings: Array<SavingsModel> = [];

  access_token = '';
  refresh_token = '';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public savingsProvider: SavingsProvider,
    public flow: FlowProvider
  ) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CustomerPage');
    this.loadSavings();
    if(localStorage.getItem('access_token')){
      this.access_token = localStorage.getItem('access_token');
    }

  }

  inviteUser() {
    console.log('send user invite');
  }

  loadSavings() {
    this.savingsProvider.loadCustomerSavings(null).subscribe((data) => {
      this.savings = data as Array<SavingsModel>;
    })
  }

}
