import { SavingsPage } from './../savings/savings';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides } from 'ionic-angular';
import { UserProvider } from '../../providers/user/user';

@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

  @ViewChild(Slides) slides: Slides;

  slidesData = [
    {
      image: "assets/img/crowd-1.png",
      icon: "assets/img/savings.png",
      bold: "Step 1:",
      text: "Set your savings goal and regular paycheck contribution.",
    },
    {
      image: "assets/img/crowd-2.png",
      bold: "Step 2:",
      text: "After continuously saving for 3 months, you gain access to a loan you can use in the case of an emergency.",
    },
    {
      image: "assets/img/crowd-3.png",
      bold: "Step 3:",
      text: "Loans will be granted at 2x the amount you saved (or up to $2,500).",
    }
  ];

  constructor(
    public navigate: NavController,
    public userProvider: UserProvider
  ) { }

  bgimage(image) {
    return `background-image: url('${image}')`;
  }

  toSaving() {
    this.navigate.push(SavingsPage);
  }

  goToStartSaving() {
    this.navigate.push('StartSavingPage');
  }

  goToCreateGoal() {
    this.navigate.push('CreateGoalPage');
  }

}


