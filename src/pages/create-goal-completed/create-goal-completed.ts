import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GoalModel } from '../../../shared/src/models/goal.model';
import { SavingsProvider } from '../../providers/savings/savings';

@IonicPage()
@Component({
  selector: 'page-create-goal-completed',
  templateUrl: 'create-goal-completed.html',
})
export class CreateGoalCompletedPage {

  goalData: GoalModel;


  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public savingsProvider: SavingsProvider
  ) {
    if (typeof this.navParams.data === 'string') {
      this.goalData = this.savingsProvider.savings.getGoals()[this.navParams.data];
    } else {
      this.goalData = this.navParams.data;
    }
  }

  goToHomePage() {
    this.navCtrl.setRoot('HomePage');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CreateGoalCompletedPage');
  }

}
