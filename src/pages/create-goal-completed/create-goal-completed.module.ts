import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CreateGoalCompletedPage } from './create-goal-completed';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    CreateGoalCompletedPage,
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(CreateGoalCompletedPage),
  ],
})
export class CreateGoalCompletedPageModule {}
