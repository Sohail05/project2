import { Component } from '@angular/core';
import { IonicPage, NavController, LoadingController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { PageIndexes } from '../../pages-index';
import { PageSettings } from '../../page-settings';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  public email;
  public password;

  constructor(
    public navCtrl: NavController,
    private authProvider: AngularFireAuth,
    public toast: ToastController,
    public loadingCtrl: LoadingController,
  ) { }

  /**
   * Perform Email & Password login.
   */
  login() {

    if (!this.email || !this.password) {
      return;
    }

    const loading = this.loadingCtrl.create();
    loading.present();

    this.authProvider.auth.signInWithEmailAndPassword(this.email, this.password).then(
      (result) => {
        console.log(result);
        // App.component has a subcription to redirect on user state changes.
      }, (error) => {
        console.log(error);
        this.toast.create({ message: error, duration: PageSettings.duration }).present();
      }
    ).catch((e) => {
      console.log(e);
    }).then(() => loading.dismiss())
  }

  /**
   * Navigate to forgot password page.
   */
  goToForgot() {
    this.navCtrl.push(PageIndexes.ForgotPassword);
  }

  /**
   * Return to previous page.
   */
  goBack() {
    this.navCtrl.pop();
  }

}
