import { Component, Input } from '@angular/core';
import { CallableProvider } from '../../providers/callable/callable';
import { AlertController } from 'ionic-angular';

@Component({
  selector: 'saved-tip',
  templateUrl: 'saved-tip.html'
})
export class SavedTipComponent {

  @Input() tip: { content };
  @Input() tipId: string;

  constructor(
    private callable: CallableProvider,
    public alertCtrl: AlertController,
  ) {
    console.log(this.tip)
    console.log('Hello SavedTipComponent Component');
  }

  remove() {
    if (this.tipId) {

      this.alertCtrl.create({
        title: 'Delete Tip',
        message: 'Are you sure you want to delete this tip?',
        buttons: [{
          text: 'No',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }, {
          text: 'Yes',
          handler: () => {
            this.performDelete();
          }
        }]
      }).present();
    }
  }

  performDelete() {
    this.callable.tips({ tip: this.tipId, remove: true })
  }



}
