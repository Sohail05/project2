import { Component, Input } from '@angular/core';

@Component({
  selector: 'notice',
  templateUrl: 'notice.html'
})
export class NoticeComponent {

  @Input() noticeData: { content: string, title: string }
  @Input() noticeId: string;

  constructor() {
    console.log('Hello NoticeComponent Component');
  }

}
