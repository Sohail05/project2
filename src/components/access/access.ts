import { Component, EventEmitter } from '@angular/core';
import { ToastController, NavController, LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { PageSettings } from '../../page-settings';
import { CallableProvider } from '../../providers/callable/callable';
import { PageIndexes } from '../../pages-index';

@Component({
  selector: 'access',
  templateUrl: 'access.html'
})
export class AccessComponent {

  public referralCode = "";
  public matched = new EventEmitter<any>();

  constructor(
    public toast: ToastController,
    public callable: CallableProvider,
    public view: ViewController,
    public nav: NavController,
    public loadingCtrl: LoadingController
  ) { }

  /**
   * Display failure to match referral code. 
   */
  displayMessage(message) {
    this.toast.create({
      message: message,
      duration: PageSettings.duration
    }).present();
  }

  /**
  * Verify Referral Code.
  */
  submitReferralCode(): void {

    const loading = this.loadingCtrl.create()
    loading.present();

    this.callable.referralCodeLookUp(this.referralCode)
      .then((result) => {
        if (result.data.error) {
          this.displayMessage(result.data.error);
        } else {
          this.matched.emit(result.data.referralCode);
        }
      })
      .catch((e) => { console.log(e) })
      .then(() => loading.dismiss());
  }

  /**
   * Mobile backdrop work around.
   * @param event
   */
  closeModal(event) {
    if (event.target && event.target.matches('div.backdrop')) {
      this.view.dismiss();
    }
  }

  NavigateToEmployerRequest() {
    this.view.dismiss();
    this.nav.push(PageIndexes.EmployerRequest)
  }

}
