import { Component, Input } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'onward-header',
  templateUrl: 'onward-header.html'
})
export class OnwardHeaderComponent {

  @Input() active;

  constructor(public nav: NavController, public auth: AngularFireAuth) { }

  brandClicked() {
    if (this.isActive()) {
      this.goToHome();
    } else {
      this.goToSplash();
    }
  }

  isActive() {

    if(this.active === false){
      return false;
    }

    if (this.auth.auth.currentUser) {
      return true;
    }
    return false;
  }

  goToSplash() {
    this.nav.setRoot("SplashPage");
  }

  goToHome() {
    this.nav.setRoot("HomePage");
  }

  goToDashboard() {
    this.nav.setRoot("DashboardPage");
  }

  goToNotifications() {
    this.nav.setRoot("NotificationsPage");
  }

  goToMessages() {
    this.nav.setRoot("MessagesPage");
  }

}
