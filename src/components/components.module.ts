import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { AccessComponent } from './access/access';
import { OnwardHeaderComponent } from './onward-header/onward-header';
import { OnwardBrandingComponent } from './onward-branding/onward-branding';
import { GoalComponent } from './goal/goal';
import { ProgressBarModule } from "angular-progress-bar"
import { TipsComponent } from './tips/tips';
import { NoticeComponent } from './notice/notice';
import { SavedTipComponent } from './saved-tip/saved-tip';

@NgModule({
	declarations: [
		AccessComponent,
		OnwardHeaderComponent,
		OnwardBrandingComponent,
		GoalComponent,
		TipsComponent,
    TipsComponent,
    NoticeComponent,
    SavedTipComponent
	],
	imports: [
		IonicModule,
		ProgressBarModule
	],
	exports: [
		AccessComponent,
		OnwardHeaderComponent,
		OnwardBrandingComponent,
		GoalComponent,
    TipsComponent,
    NoticeComponent,
    SavedTipComponent
	],
	entryComponents: [
		AccessComponent
	]
})
export class ComponentsModule { }
