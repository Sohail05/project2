import { Component, Input } from '@angular/core';
import { GoalModel } from './../../../shared/src/models/goal.model';

@Component({
  selector: 'goal',
  templateUrl: 'goal.html'
})
export class GoalComponent {

  @Input() goal = new GoalModel()
  @Input() isButton = true;

  constructor() { }

  getProgress(): number {

    if (this.goal && this.goal.amount && this.goal.targetAmount) {
      const val = this.goal.amount / this.goal.targetAmount * 100;
      return val;
    } else {
      return 0;
    }

  }

}
