import { Component } from '@angular/core';

/**
 * Generated class for the OnwardBrandingComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'onward-branding',
  templateUrl: 'onward-branding.html'
})
export class OnwardBrandingComponent {

  text: string;

  constructor() {
    console.log('Hello OnwardBrandingComponent Component');
    this.text = 'Hello World';
  }

}
