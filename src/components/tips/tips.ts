import { Component, Input } from '@angular/core';
import { CallableProvider } from '../../providers/callable/callable';

@Component({
  selector: 'tips',
  templateUrl: 'tips.html'
})
export class TipsComponent {

  @Input() tip: { content: string, likes: { [key: string]: number } };
  @Input() userId: string;
  @Input() tipId: string;
  @Input() saved = false;

  constructor(
    private callable: CallableProvider
  ) { }

  like(val: number) {
    this.callable.tips({ like: val, tip: this.tipId })
    const obj = {}
    obj[this.userId] = val;
    this.tip['likes'] = obj;
  }

  isLiked() {
    if (this.tip.likes && this.tip.likes[this.userId]) {
      return this.tip.likes[this.userId] == 1;
    }
    return false
  }

  isDisliked() {

    if (this.tip.likes && this.tip.likes[this.userId]) {
      return this.tip.likes[this.userId] == -1;
    }
    return false
  }

  saveTip() {
    if (this.saved) {
      return;
    }
    this.callable.tips({ save: true, tip: this.tipId })
    this.saved = true;
  }

}
