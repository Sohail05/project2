import * as crypto from 'crypto';
export class FlowController {

    /**
     * Referral Code generator for new customers.
     * First four characters: the institute name.
     * Next two characters: the state code.
     * @param customer 
     */
    generateReferralCode = (customer) => {
        return customer.name.toLowerCase().slice(0, 4) + customer.stateCode.toLowerCase().slice(0, 2);
    };

    /**
     * Fingerprint generator
     * Onward uid with secret
     */
    generateFingerprint = (oid) => {
        const hmac = crypto.createHmac('sha256', 'onward_secret_salt');
        hmac.update(oid);
        return hmac.digest('hex');
    };


    /**
     * Singature generator
     * Sps object ID
     */
    generateSignature = (oid, algo, client) => {
        const hmac = crypto.createHmac(algo, client.client_secret);
        hmac.update(`${oid}+${client.client_id}`);
        return hmac.digest('hex');
    };
    

}
