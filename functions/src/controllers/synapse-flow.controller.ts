import * as request from 'request-promise';
import * as admin from 'firebase-admin';
import { callable } from '../../../shared/src/callable';
import { DBLogger } from '../services/db-log';
import { SynapseRequestHeader } from '../classes/synapse-request-header';
import { SynapseUserCreateDTO } from '../../../shared/src/dto/synapse-user.dto';
import { FlowController } from './flow.controller';
import { configInterface } from '../interface/config';
import { Onward } from '../../../shared/src/types/types';

export class SynapseFlowController {

    headers: SynapseRequestHeader;

    constructor(private app: admin.app.App, private config: configInterface) {
        this.headers = new SynapseRequestHeader(this.config.synapse);
    }

    /**
     * Controller HTTP Request handler.
     */
    requestHandler = (options: request.Options) => {
        const opt = Object.assign({ headers: this.headers.headers(), json: true }, options);
        return request(opt).catch((error) => { new DBLogger(this.app.database()).error(options['uri'], { error, options }); console.error(options['uri'], error); return error })
    }

    /**
     * Create new SPS user.
     */
    create: (data: { body: SynapseUserCreateDTO }, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser('|' + new FlowController().generateFingerprint(context.auth.uid));

        data.body.documents.map(d => Object.assign(d, { ip: context.rawRequest.ip }))
        data.body['extra'] = {
            supp_id: context.auth.uid,
            cip_tag: 1,
            is_business: false
        }

        const options = {
            method: 'POST',
            uri: this.config.synapse.endpoint + 'users',
            body: data.body,
        }

        const callback = (response) => {
            if (response['error']) {
                throw response.error;
            }
            const obj = {
                sps: {
                    id: response["_id"],
                    permission: response["permission"],
                }
            }
            return this.app.database().ref('/users/' + context.auth.uid).update(obj).then(() => {
                return response;
            });
        }

        return this.requestHandler(options).then(callback);
    }

    /**
     * Pass Refresh token to Retrieve OAuth.
     */
    oauth: (data, context) => any | Promise<any> = (data, context) => {
        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser('|' + new FlowController().generateFingerprint(context.auth.uid))
        const options: request.Options = {
            method: 'POST',
            uri: this.config.synapse.endpoint + 'oauth/' + data['_id'],
            body: { refresh_token: data['refresh_token'] }
        };

        return this.requestHandler(options);
    }

    /**
     * Create user's saving node.
     */
    createSavingNode: (data, context) => any | Promise<any> = (data, context) => {
        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data['oauth_key'] + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options: request.Options = {
            method: 'POST',
            uri: this.config.synapse.endpoint + 'users/' + data['user_id'] + '/nodes',
            body: {
                "type": "IB-DEPOSIT-US",
                "info": {
                    "nickname": "Onward Savings"
                },
                "extra": {
                    "supp_id": context.auth.uid
                }
            }
        };

        const callback = (body) => {
            if (body['error'] || !body.nodes[0]) {
                return body.error;
            }

            const nid = body.nodes[0]['_id'];
            const balance = body.nodes[0]['info']['balance']['amount'];
            const obj = {
                saving: { id: nid, balance, oauth_key: data['oauth_key'] }
            }
            return this.app.database().ref('/users/' + context.auth.uid + '/sps').update(obj).then(() => {
                return body;
            });
        }

        return this.requestHandler(options).then(callback);
    }

    /**
     * Get oauth response with access token.
     */
    getAccessToken: (data, context) => any | Promise<any> = (data, context) => {
        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser('|' + new FlowController().generateFingerprint(context.auth.uid));

        return this.getUserInfo(data, context).then((snapshot) => {
            const options = {
                method: 'GET',
                uri: this.config.synapse.endpoint + 'users/' + snapshot.val()['sps']['id'],
                qs: { full_dehydrate: 'no' },
            }
            const callback = (userData) => {
                return userData['error'] ? userData['error'] : this.oauth(userData, context);
            }

            return this.requestHandler(options).then(callback);

        })
    }

    /**
     * Retrieve SPS user.
     */
    find: (data: callable.DTO.GetSpsUser, context) => any | Promise<any> = (data, context) => {
        this.headers.setIP(context.rawRequest.ip);
        const options: request.Options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users/' + data.uid,
            qs: { full_dehydrate: data.full_dehydrate },
        };

        return this.requestHandler(options);
    }

    /**
     * Retrieve all users.
     */
    findAll: (data: callable.DTO.GetSpsUsers, context) => any | Promise<any> = (data, context) => {
        this.headers.setIP(context.rawRequest.ip);
        const options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users',
            qs: data,
        };
        return this.requestHandler(options);
    }

    /**
     * @debug
     */
    dummyTransaction: (data, context) => any | Promise<any> = (data, context) => {
        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data['access_token'] + '|' + new FlowController().generateFingerprint(context.auth.uid));
        const options: request.Options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users/' + data['user_id'] + '/nodes/' + data['node_id'] + 'dummy-tran' + '?is_credit=yes',
        };
        return this.requestHandler(options);
    }

    /**
     * Sum of all nodes.
     */
    balance: (data, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip)
        this.headers.setUser(data['oauth_key'] + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options: request.Options = {
            method: 'GET',
            uri: this.config.synapse.endpoint + 'users/' + data['user_id'] + '/nodes',
        };

        const callback = (body) => {
            return body['error'] ?
                body.error : { node: body.nodes.map((node) => node['info']['balance']['amount']) }
        }

        return this.requestHandler(options).then(callback);
    }

    /**
     * Create beneficiary account node.
     */
    createAchNode: (data: callable.DTO.createAchNode, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data.oauth_key + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options: request.Options = {
            method: 'POST',
            uri: this.config.synapse.endpoint + 'users/' + data.uid + '/nodes',
            body: {
                "type": "ACH-US",
                "info": {
                    "nickname": data.nickname,
                    "account_num": data.account_num,
                    "routing_num": data.routing_num,
                    "type": "PERSONAL",
                    "class": "CHECKING"
                },
                "is_active": true,
                "extra": {
                    "supp_id": context.auth.uid
                }
            },
        };

        const callback = (body) => {
            if (body['error']) {
                return body.error;
            }
            const nodeObj = {
                beneficiary: { id: body.nodes[0]['_id'] }
            }
            return this.app.database().ref('/users/' + context.auth.uid + '/sps/nodes/' + body.nodes[0]['_id']).set(nodeObj).then(() => {
                return body;
            });
        }

        return this.requestHandler(options).then(callback);
    }

    /**
     * Get ACH Nodes
     */
    getAchNodes: (data: callable.DTO.getAchNodes, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data['oauth_key'] + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options: request.Options = {
            method: 'GET',
            uri: this.config.synapse.endpoint + 'users/' + data['uid'] + '/nodes',
            qs: { type: 'ACH-US' },
        };

        const callback = (body: { nodes: Array<any> }) => {

            const nodes = {}
            body.nodes.forEach((node) => {
                nodes[node._id] = { id: node._id, balance: node.info.balance.amount, nickname: node.info.nickname }
            })

            return this.app.database().ref('/users/' + context.auth.uid + '/sps/nodes/').set(nodes).then(() => {
                return body;
            });
        }

        return this.requestHandler(options).then(callback);
    }


    /**
    * Delete ACH Nodes
    */
    deleteAchNode: (data, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data['oauth_key'] + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options: request.Options = {
            method: 'DELETE',
            uri: this.config.synapse.endpoint + 'users/' + data['uid'] + '/nodes/' + data['nid'],
        };

        const callback = (body: { nodes: Array<any> }) => {
            return this.app.database().ref('/users/' + context.auth.uid + '/sps/nodes/' + data['nid']).remove();
        }

        return this.requestHandler(options).then(callback);
    }

    /**
     * Retrieve onward user data.
     */
    getUserInfo: (data, context) => Promise<any> = (data, context) => {
        return this.app.database().ref('/users/' + context.auth.uid).once('value')
    }

    /**
     * withdrawal transaction.
     * Todo: Pre-Check Available funds vs Amount.
     */
    withdrawalTransaction: (data: { oauth_key, uid, snid, dnid, amount, note }, context) => any | Promise<any> = async (data, context) => {

        try {
            const userInfoSnapshot = await this.getUserInfo(data, context);

            const userData: Onward.user = userInfoSnapshot.val()
            if (userData.sps.saving.balance < data.amount) {
                return { 'error': 'Insufficient Funds' }
            }

            if (userData.sps.saving.monthly_withdrawals_remaining <= 0) {
                return { 'error': 'Remaining monthly withdrawals: 0' }
            }

            this.headers.setIP(context.rawRequest.ip);
            this.headers.setUser(data.oauth_key + '|' + new FlowController().generateFingerprint(context.auth.uid));

            const options: request.Options = {
                method: 'POST',
                uri: this.config.synapse.endpoint + 'users/' + data.uid + '/nodes/' + data.snid + '/trans',
                body: {
                    "to": {
                        "type": "ACH-US",
                        "id": data.dnid
                    },
                    "amount": {
                        "amount": data.amount,
                        "currency": "USD"
                    },
                    "extra": {
                        "ip": context.rawRequest.ip,
                        "same_day": false,
                        "note": data.note,
                        "supp_id": context.auth.uid
                    },
                    "fees": [{
                        "fee": -0.05,
                        "note": "Onward facilitator fee",
                        "to": {
                            "id": this.config.synapse.node_id
                        }
                    }]
                },
            };

            const callback = (body) => {
                return body['error'] ? body.error : body;
            }

            return this.requestHandler(options).then(callback);

        } catch (error) {
            return error['error'] ? error.error : error;
        }
    }

    /**
     * Update SPS user.
     */
    update: (data: any, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);

        const options = {
            method: 'patch',
            uri: this.config.synapse.endpoint + data.route,
            body: data,
        };

        const callback = (body) => {
            if (body['error']) {
                return body.error;
            }
            const sps = {
                id: body["_id"],
                permission: body["permission"],
            }

            return this.app.database().ref('/users/' + context.auth.uid + '/sps').update(sps);
        }

        return this.requestHandler(options).then(callback)
    }

    /**
     * Verify micro transactions amounts.
     */
    verifyAch: (data: any, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data.oauth_key + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options = {
            method: 'PATCH',
            uri: this.config.synapse.endpoint + 'users/' + data.uid + '/nodes/' + data.nid,
            body: { "micro": data.micro }
        };

        return this.requestHandler(options)
    }


    /**
     * Create Subnet for Saving Node.
     */
    createSubnet: (data: { ouid, suid, nid, oauth_key }, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP('127.0.0.1');
        this.headers.setUser(data.oauth_key + '|' + new FlowController().generateFingerprint(data.ouid));

        const options = {
            method: 'POST',
            uri: this.config.synapse.endpoint + 'users/' + data.suid + '/nodes/' + data.nid + '/subnets',
            body: { "nickname": "Payroll" }
        };

        const callback = (body) => {
            if (body['error']) {
                return body.error;
            }
            console.log(body);

            const subnetData = {
                account_num: body.account_num,
                routing_num: body.routing_num
            }

            return this.app.database().ref('/users/' + data.ouid + '/sps/saving').update(subnetData);
        }


        return this.requestHandler(options).then(callback)

    }


    /**
     * Retrieve Transactions.
     */
    transactionHistory: (data: any, context) => any | Promise<any> = (data, context) => {

        this.headers.setIP(context.rawRequest.ip);
        this.headers.setUser(data.oauth_key + '|' + new FlowController().generateFingerprint(context.auth.uid));

        const options = {
            method: 'GET',
            uri: this.config.synapse.endpoint + 'users/' + data.uid + '/nodes/' + data.nid + '/trans',
            qs: { page: data.page || 1, per_page: 20 }
        };

        return this.requestHandler(options)
    }

    /******************************** Patch Fixes  **********************************************/

    getUserSynapsePatch = (uid, suid) => {
        this.headers.setIP('127.0.0.1');
        this.headers.setUser('|' + new FlowController().generateFingerprint(uid));
        const options = {
            method: 'GET',
            uri: this.config.synapse.endpoint + 'users/' + suid,
            qs: { full_dehydrate: 'no' },
        }
        return this.requestHandler(options);
    }

    /**
     * Pass Refresh token to Retrieve OAuth.
     */
    oauthPatch = (userData, uid, suid) => {
        this.headers.setIP('127.0.0.1');
        this.headers.setUser('|' + new FlowController().generateFingerprint(uid))
        const options: request.Options = {
            method: 'POST',
            uri: this.config.synapse.endpoint + 'oauth/' + suid,
            body: { refresh_token: userData['refresh_token'] }
        };
        return this.requestHandler(options);
    }

    patchFix = (suid, uid, token) => {
        this.headers.setIP('127.0.0.1');
        this.headers.setUser(token + '|' + new FlowController().generateFingerprint(uid))
        const options: request.Options = {
            method: 'PATCH',
            uri: this.config.synapse.endpoint + 'users/' + suid,
            body: { update: { public_note: 'PatchFix' } }
        };

        return this.requestHandler(options);
    }

}