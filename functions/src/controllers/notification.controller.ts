
import * as mailer from 'nodemailer';
import * as striptags from 'striptags';
import * as formatter from 'libphonenumber-js';
import * as Twilio from 'twilio';
import { welcomeContent } from '../content';

export class NotificationController {

    mailTransport;

    constructor(private config) {
        this.mailTransport = mailer.createTransport(`smtps://${this.config.gmail.email}:${this.config.gmail.password}@smtp.gmail.com`);
    }

    /**
     * Sent email.
     * @param subject 
     * @param text 
     * @param email 
     */
    sendMail(subject: string, text: string, email: string) {

        console.log('Sending email:', email, subject);

        const mailOptions = {
            from: `Onward Financial <admin@getonward.org>`,
            to: email,
            subject: subject,
            text: `${text}`
        };

        return this.mailTransport.sendMail(mailOptions)
            .then(() => {
                console.log('New welcome email sent to:' + email);
            }, (error) => {
                console.error(error);
            });
    }

    /**
     * Send SMS.
     */
    sendSMS = (payload: { phone, body }) => {
        const client = new Twilio(this.config.twilio.sid, this.config.twilio.auth_token);

        payload.phone = payload.phone.replace('+1', '');
        const phone = formatter.format(payload.phone.replace('+1', ''), 'US', 'International').replace(/ /g, '');
        const notificationOpts = {
            toBinding: JSON.stringify({
                binding_type: 'sms',
                address: phone,
            }),
            body: striptags(payload.body),
        };

        console.log('Sending SMS:', payload.phone, payload.body);

        return client.notify
            .services('IS89c8ca069e87416bf9217b56199ec63a')
            .notifications.create(notificationOpts)
    }

    /**
     * Send Welcome Email.
     */
    sendWelcomeEmail = (name, email) => {
        const subject = `Welcome from Onward Financial!`;
        const text = welcomeContent(name)
        this.sendMail(subject, text, email);
    }

    /**
     * Send Welcome SMS.
     */
    sendWelcomeSMS = (name, phone) => {
        const body = welcomeContent(name)
        this.sendSMS({ phone, body });
    }

    /**
    * Send Welcome Notifications.
    */
    sendWelcomeNotification = (name, email, phone) => {
        this.sendWelcomeSMS(name, phone)
        this.sendWelcomeEmail(name, email)
    }



} 