import { CallableContext } from "firebase-functions/lib/providers/https";
import * as admin from 'firebase-admin';
import { UserDTO } from '../../../shared/src/dto/user.dto';
import { callable } from '../../../shared/src';
import { DBLogger } from '../services/db-log';
import { configInterface } from '../interface/config';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';
import { Onward } from "../../../shared/src/types/types";
import { EmployerRequestDTO } from '../../../shared/src/dto/employer-request.dto';
import { Request, Response } from 'express';

export class OnwardController {

    constructor(private app: admin.app.App, private config: configInterface) { }

    /**
     * Insert new customer. 
     */
    createCustomer: (data: callable.DTO.CreateCustomer, context: CallableContext) => any | Promise<any> = (data, context) => {
        return this.app.database().ref('/customers').push(data).then((result: admin.database.ThenableReference) => {
            return result.key;
        })
    }

    /**
     * Insert new user profile. 
     */
    createUser: (data: any, context: CallableContext) => any | Promise<any> = (data: UserDTO, context) => {

        delete data['confirm_password'];
        delete data['password'];

        return this.app.database().ref('/users').child(context.auth.uid).set(data).then(() => {
            return context.auth.uid;
        })
    }

    /**
     * Insert new savings goal.
     * todo remove
     */
    createSavings: (data: any, context: CallableContext) => any | Promise<any> = (data, context) => {
        data['userId'] = context.auth.uid;
        data['confirmation'] = 'pending';
        return this.app.database().ref('/savings').push(data).then(() => { return data });
    }

    /**
     * Payroll Payment Cycle.
     */
    paymentCycle: (data: any, context: CallableContext) => any | Promise<any> = (data, context) => {
        return this.app.database().ref('/savings').orderByChild('userId').equalTo(context.auth.uid).once("value", (snapshot) => {
            console.log(snapshot.val());
            return snapshot.val();
        })
    }

    /**
     * Payroll
     */
    payroll: (req: Request, resp: Response) => void = async (req, res) => {

        try {

            // e.g. {employerId: "LINC-1"}
            const params = req.query;

            const force = params.force || false;

            const payrollData = await this.generatePayroll(params.empoyerId, force, true);

            if (payrollData.content.length === 0) {
                res.send('No Content To Generate');
                return
            }

            await this.app.database().ref('/payrolls/' + params.employerId).push({
                date: new Date().getTime(),
                content: payrollData.content,
                count: payrollData.content.length,
                conserns: payrollData.conserns
            });

            res.contentType('text/csv')
            res.send(payrollData.content);
        } catch (e) {
            console.log(e);
            new DBLogger(this.app.database()).error('OnwardController:payroll', e);
            res.status(400);
            res.send('Invalid Request');
        }
    }

    /**
     * Payroll Archive
     */
    payrollArchive: (req: Request, resp: Response) => void = async (req, res) => {

        try {
            // employerId & payrollId
            const params = req.query;
            const payrollSnapshot = await this.app.database().ref('/payrolls/' + params.employerId + '/' + params.payrollId).once('value');
            const payroll = payrollSnapshot.val();
            res.contentType('text/csv')
            res.send(payroll.content);
        } catch (e) {
            console.log(e);
            new DBLogger(this.app.database()).error('OnwardController:payrollArchive', e);
            res.status(400);
            res.send('Invalid Request');
        }
    }



    /**
     * Available Funds evaluation for Withdrawal.
     * Todo: Add loans calculation
     * Todo: Get Balance from Synapse.
     */
    availableFunds: (data: any, context: CallableContext) => any | Promise<any> = (data, context) => {

        //Get SPS ID
        return this.app.database().ref('/users/' + context.auth.uid + '/sps').once("value")
            .catch((error) => this.handleError(error, 'availableFunds: Retrieve SPS Data'))
            .then((sps: DataSnapshot) => {
                return { amount: sps.val()['saving']['balance'], remaining: sps.val()['saving']['monthly_withdrawals_remaining'] }
            })
    }


    /**
     * Employer Request
     */
    employerRequest: (data: EmployerRequestDTO, context: CallableContext) => any | Promise<any> = (data, context) => {

        const request = {
            ...data,
            createdAt: new Date().toISOString()
        }

        return this.app.database().ref('/employerRequest').push(request)
            .then((result: admin.database.ThenableReference) => {
                return result.key;
            })
    }

    /**
     * Account Closure Request
     */
    accountClosureRequest: (data: string, context: CallableContext) => any | Promise<any> = (data, context) => {

        const request = {
            reason: data,
            user: context.auth.uid
        }

        return this.app.database().ref('/accountClosureRequest').push(request)
            .then((result: admin.database.ThenableReference) => {
                return result.key;
            })
    }

    /**
     * Handle tips likes/dislikes & favorites;
     */
    tips: (data: { like?: number, tip: string, save?: boolean, remove?: boolean }, context: CallableContext) => any | Promise<any> = async (data, context) => {

        if (!data.tip) {
            return {};
        }

        const response = { tip: data.tip };

        try {

            if (data.like) {
                await this.app.database().ref('/tips/' + data.tip + '/likes/' + context.auth.uid).set(data.like)
                response['like'] = data.like;
            }

            if (data.remove) {
                await this.app.database().ref('/saved/' + context.auth.uid + '/tips/' + data.tip).remove()
                response['remove'] = data.remove;
                return response;
            }

            if (data.save) {
                const tipSnapshot = await this.app.database().ref('/tips/' + data.tip).once("value")
                const val = tipSnapshot.val();

                if (val && val['content']) {
                    await this.app.database().ref('/saved/' + context.auth.uid + '/tips/' + data.tip).set({ content: val['content'] })
                } else {
                    console.warn('No tip:' + data.tip);
                }

                response['save'] = true;
            }

        } catch (e) {
            return { error: true }
        }

        return response;
    }


    /**
   * Payroll
   */
    masterList: (data, context: CallableContext) => void = async (data, context) => {

        try {

            const userDataSnap = await this.getUserInfo(data, context);
            const userData: Onward.user = userDataSnap.val();
            console.log(userData);

            const payrollData = await this.generatePayroll(userData.employerId, false, false);
            const header = `"Employee ID","Legal Name","Ach Routing","Account Number",Deduction,Frequency`;
            return { payroll: { content: payrollData.content, header } }
        } catch (e) {
            console.log(e);
            new DBLogger(this.app.database()).error('OnwardController:masterList', e);
            return { error: e }
        }
    }

    /**
     * Retrieve onward user data.
     */
    getUserInfo: (data, context) => Promise<admin.database.DataSnapshot> = (data, context) => {
        return this.app.database().ref('/users/' + context.auth.uid).once('value')
    }

    /**
     * Log errors to console & store to database.
     */
    handleError = (error, name?) => {
        new DBLogger(this.app.database()).error(name || 'OnwardController', error);
        console.error(name || 'OnwardController', error);
    }

    employerFileList = async (data, context) => {
        const eidSnap = await this.app.database().ref(`/users/${context.auth.uid}/employerId`).once('value');
        const employerId = eidSnap.val();
        const payrollSnap = await this.app.database().ref('/payrolls/' + employerId).once('value');
        return payrollSnap.val();
    }

    /**
     * Check existing and status for referral codes.
     */
    referralCodeLookUp = async (data: string, context) => {
        const refSnap = await this.app.database().ref('/referralCodes').orderByChild('referralCode').equalTo(data.toLocaleLowerCase()).once('value');

        const snapData = refSnap.val();
        let refData: Onward.referralCode;
        console.log("Referral Code Look Up:", data.toLocaleLowerCase());
        if (snapData && snapData.hasOwnProperty(data.toLocaleLowerCase())) {
            refData = refSnap.val()[data.toLocaleLowerCase()];
        } else {
            return { error: "The provided Referral Code does not exist." }
        }

        if (refData && refData.active) {
            return { referralCode: refData };
        }

        if (refData && !refData.active) {
            return { error: "The provided Referral Code has already been used.", referralCode: refData }
        }

        return { error: "The provided Referral Code does not exist." }
    }


    generatePayroll = async (employerId: string, force: boolean, displayRouting: boolean): Promise<{ content: Array<string>, conserns: any }> => {

        if (!employerId) {
            throw Error("Invalid EmployerId Provided.")
        }

        const userSnapshot = await this.app.database().ref('/users')
            .orderByChild('employerId')
            .equalTo(employerId)
            .once("value");

        const savingsSnapshot = await this.app.database().ref('/savings')
            .orderByChild('employerId')
            .equalTo(employerId)
            .once("value");

        const userKeys = Object.keys(userSnapshot.val())
        const users = userSnapshot.val()
        const savings = savingsSnapshot.val()


        let header = '';
        if (displayRouting) {
            header = `"Employee ID","Legal Name","Ach Routing","Account Number",Deduction,Frequency`;
        } else {
            header = `"Employee ID","Legal Name",Deduction,Frequency`;
        }

        const content = [];

        const conserns = {
            sps: 0,
            saving: 0,
            subnet: 0,
            unverified: 0
        }

        if (userKeys.length === 0) {
            return { content: [], conserns: {} }
        }

        userKeys.forEach((key) => {

            if (!savings[key]) {
                console.log("Missing Savings for Key:", key);
                return
            }

            if (!(savings[key].dirty) && force == false) {
                return;
            }

            this.app.database().ref('/savings/' + key + '/dirty').transaction((dirty) => {
                return false;
            });

            const payroll: Onward.user & Onward.Savings = Object.assign(users[key], savings[key])
            if (payroll) {
                if (displayRouting) {
                    if (!payroll.sps) {
                        conserns.sps = conserns.sps + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}","Registration In Process","Registration In Process",${payroll.totalDeduction},${payroll.frequency}`)
                    } else if (payroll.sps.permission !== 'SEND-AND-RECEIVE') {
                        conserns.unverified = conserns.unverified + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}","Verification Pending","Verification Pending",${payroll.totalDeduction},${payroll.frequency}`)
                        return
                    } else if (!payroll.sps.saving) {
                        conserns.saving = conserns.saving + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}","Pending Approval","Pending Approval",${payroll.totalDeduction},${payroll.frequency}`)
                    } else if (!payroll.sps.saving.routing_num) {
                        conserns.subnet = conserns.subnet + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}","Approval In Process","Approval In Process",${payroll.totalDeduction},${payroll.frequency}`)
                    } else {
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}",${payroll.sps.saving.routing_num.ach},${payroll.sps.saving.account_num},${payroll.totalDeduction},${payroll.frequency}`)
                    }
                } else {
                    if (!payroll.sps) {
                        conserns.sps = conserns.sps + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}",${payroll.totalDeduction},${payroll.frequency}`)
                    } else if (payroll.sps.permission !== 'SEND-AND-RECEIVE') {
                        conserns.unverified = conserns.unverified + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}",${payroll.totalDeduction},${payroll.frequency}`)
                        return
                    } else if (!payroll.sps.saving) {
                        conserns.saving = conserns.saving + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}",${payroll.totalDeduction},${payroll.frequency}`)
                    } else if (!payroll.sps.saving.routing_num) {
                        conserns.subnet = conserns.subnet + 1;
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}",${payroll.totalDeduction},${payroll.frequency}`)
                    } else {
                        content.push(`${users[key].employeeId},"${payroll['legal_name']}",${payroll.totalDeduction},${payroll.frequency}`)
                    }
                }
            }
        })

        return { content, conserns };

    }


}

