import * as request from 'request-promise';
import * as admin from 'firebase-admin';
import { DBLogger } from '../services/db-log';
import { SynapseRequestHeader } from '../classes/synapse-request-header';
import { configInterface } from '../interface/config';
import { Request, Response } from 'express';
import { SynapseUserService } from '../service.ts/synapse-user';
import { SynapseAccountService } from '../service.ts/synapse-account';
import { Onward } from '../../../shared/src/types/types';

export class SynapseSyncController {

    headers: SynapseRequestHeader;

    constructor(private app: admin.app.App, private config: configInterface) {
        this.headers = new SynapseRequestHeader(this.config.synapse);
    }

    /**
     * Controller HTTP Request handler.
     */
    requestHandler = (options: request.Options) => {
        const opt = Object.assign({ headers: this.headers.headers(), json: true }, options);
        return request(opt).catch((error) => { new DBLogger(this.app.database()).error(options['uri'], { error, options }); console.error(options['uri'], error); return error })
    }

    /**
     * 
     */
    syncUser: (req: Request, resp: Response) => void = async (req, res) => {

        const params = req.query;

        if (!params.suid) {
            res.send("Missing suid");
            return
        }

        const userSnapshot = await this.app.database().ref('/users')
            .orderByChild('/sps/id')
            .equalTo(params.suid)
            .once("value");

        const userKey = Object.keys(userSnapshot.val()).shift()

        console.log(userKey);

        if (!userKey) {
            res.send("No user found.");
            return
        }

        try {
            const result = await new SynapseUserService(this.app, this.config).getUser(userKey, params.suid);
            const update = await this.app.database().ref('/users/' + userKey + '/sps/permission').update(result.permission, (error) => {
                console.log(error);
            });
            res.send(result);
        } catch (e) {
            res.send("Error");
        }
    }
    //5beb18540da9d400c076d5e0
    /**
     * 
     */
    syncNode: (req: Request, resp: Response) => void = async (req, res) => {
        const params = req.query;

        if (!params.suid) {
            res.send("Missing suid");
            return
        }

        const userSnapshot = await this.app.database().ref('/users')
            .orderByChild('/sps/id')
            .equalTo(params.suid)
            .once("value");

        const userKey = Object.keys(userSnapshot.val()).shift()
        const userData: Onward.user = userSnapshot.val()[userKey];

        console.log(userKey);

        if (!userKey) {
            res.send("No user found.");
            return
        }

        try {
            console.log("User Key", userKey);
            console.log("User SUID", userData.sps.id);
            const userResponse = await new SynapseUserService(this.app, this.config).getUser(userKey, params.suid);
            console.log("User Response", userResponse);
            const oauthResponse = await new SynapseUserService(this.app, this.config).getOAuth(userKey, params.suid, userResponse.refresh_token);
            console.log("OAuth Response", oauthResponse);
            const savingNodesResponse = await new SynapseAccountService(this.app, this.config).getSavingNodes(userKey, params.suid, userData.sps.saving.id, oauthResponse.oauth_key);
            console.log("Saving Nodes Response", savingNodesResponse);

            if (!savingNodesResponse.nodes || savingNodesResponse.nodes.length === 0) {
                res.send("No Nodes");
            }

            const nid = savingNodesResponse.nodes[0]['_id'];
            const balance = savingNodesResponse.nodes[0]['info']['balance']['amount'];
            const obj = {
                saving: { id: nid, balance, oauth_key: oauthResponse.oauth_key }
            }

            await this.app.database().ref('/users/' + userKey + '/sps').update(obj);

            res.send(savingNodesResponse);
        } catch (e) {
            res.send(e);
        }

    }

    /**
     * 
     */
    syncSubnet: (req: Request, resp: Response) => void = async (req, res) => {

        const params = req.query;

        if (!params.suid) {
            res.send("Missing suid");
            return
        }

        const userSnapshot = await this.app.database().ref('/users')
            .orderByChild('/sps/id')
            .equalTo(params.suid)
            .once("value");

        const userKey = Object.keys(userSnapshot.val()).shift()
        const userData: Onward.user = userSnapshot.val()[userKey];

        console.log(userKey);

        if (!userKey) {
            res.send("No user found.");
            return
        }

        try {
            console.log("User Key", userKey);
            console.log("User SUID", userData.sps.id);
            console.log("User NID", userData.sps.saving.id);
            const userResponse = await new SynapseUserService(this.app, this.config).getUser(userKey, params.suid);
            console.log("User Response", userResponse);
            const oauthResponse = await new SynapseUserService(this.app, this.config).getOAuth(userKey, params.suid, userResponse.refresh_token);
            console.log("OAuth Response", oauthResponse);
            const subnetResponse = await new SynapseAccountService(this.app, this.config).getSubnets(userKey, params.suid, userData.sps.saving.id, oauthResponse.oauth_key);
            console.log("Subnet Response", subnetResponse);

            const subnetObject = {
                account_num: subnetResponse.subnets[0].account_num,
                routing_num: subnetResponse.subnets[0].routing_num
            }

            const update = await this.app.database().ref('/users/' + userKey + '/sps/saving').update(subnetObject);
            res.send(subnetResponse);
        } catch (e) {
            res.send(e);
        }




    }

    /**
     * 
     */
    syncAll: (req: Request, resp: Response) => void = (req, res) => {
        res.send('wip')
    }


}