import * as admin from 'firebase-admin';
import * as request from 'request-promise';
import { SynapseFlowController } from './synapse-flow.controller';
import { Request, Response } from 'express';
import { DBLogger } from '../services/db-log';
import { NotificationController } from './notification.controller';
export class SynapseController {

    constructor(private app: admin.app.App, private config) { }

    /**
     * Synapse Object Synapse webhook.
     */
    usersListener: (req: Request, resp: Response) => void = (req, res) => {

        if (req.body["webhook_meta"]["function"] && req.body["webhook_meta"]["function"] === "USER|PATCH") {
            const permission = { permission: req.body["_rest"]['permission'] }
            this.app.database().ref('/users/' + req.body["_rest"]['extra']['supp_id'] + '/sps').update(permission);
        }

        this.app.database().ref('/subscriptions/users')
            .push({ meta: req.body["webhook_meta"], obj: req.body["_rest"] });
        res.sendStatus(200);
        return;
    }

    /**
     * Synapse Transaction Object Webhook.
     */
    transactionsListener: (req, res) => void = (req, res) => {

        if (
            req.body.to.type === "IB-DEPOSIT-US"
            && req.body.recent_status.status === "SETTLED"
            && req.body.from.meta.type === "ACH"
        ) {

            const amount = req.body.amount.amount;
            const fees = req.body.fees.reduce((a, b) => (a.fee | a) + b.fee, 0);
            const incomingPayrollAmount = amount - fees;

            const ref = this.app.database().ref('/savings')
                .orderByChild('id').equalTo(req.body.to.id.$oid).ref;

            const payroll = {
                date: new Date(req.body.extra.created_on.$date).toISOString(),
                amount: incomingPayrollAmount,
                fees: fees
            }

            console.log(payroll);
            console.log(req.body.to.id.$oid);
            console.log(req.body.to.user._id.$oid);

            this.app.database().ref('/savings')
                .orderByChild('id').equalTo(req.body.to.id.$oid).once('value', (snap) => {
                    const oid = Object.keys(snap.val()).shift();
                    console.log(oid);
                    this.app.database().ref('/savings/' + oid + '/payroll').push(payroll)
                })
        }

        this.app.database().ref('/subscriptions/transactions')
            .push({ meta: req.body["webhook_meta"], obj: req.body["_rest"] });
        res.sendStatus(200);
        return;
    }

    /**
     * Synapse Node Object Webhook.
     */
    nodesListener: (req, res) => void = (req, res) => {

        if (req.body["webhook_meta"]["function"] && req.body["webhook_meta"]["function"] === "NODE|PATCH") {

            if (req.body["_rest"]["type"] === "IB-DEPOSIT-US" && req.body["_rest"]["extra"]["supp_id"]) {
                const supp_id = req.body["_rest"]["extra"]["supp_id"];
                const amount = req.body["_rest"]['info']['balance']['amount'];
                const monthly_withdrawals_remaining = req.body["_rest"]['info']['monthly_withdrawals_remaining'];
                this.app.database().ref('/users/' + supp_id + '/sps/saving/').update({
                    balance: amount,
                    monthly_withdrawals_remaining: monthly_withdrawals_remaining
                }).then(() => {
                    console.log('Updated balance for ' + req.body["_rest"]['user_id']);
                });
            }
        }

        this.app.database().ref('/subscriptions/nodes')
            .push({ meta: req.body["webhook_meta"], obj: req.body["_rest"] });
        res.sendStatus(200);
        return;
    }

    /**
     * Listen for SPS subcriptions.
     */
    subcriptionListener: (req, res) => void = (req, res) => {

        if (req.body["webhook_meta"]["function"]) {
            const type = (<string>req.body["webhook_meta"]["function"]).split('|').shift();
            if (type) {
                this.app.database().ref('/subscriptions/' + type).push(req.body);
            }
        }

        this.app.database().ref('/subscriptions/all')
            .push({ meta: req.body["webhook_meta"], obj: req.body["_rest"] });
        res.send(req.body);
        return;
    };

    /**
     * @debug
     */
    /*gateway: (req, resp) => void = (req, res) => {

        const headers = Object.assign({
            "X-SP-GATEWAY": this.config.synapse.client_id + '|' + this.config.synapse.client_secret,
            "X-SP-USER-IP": req.ip
        }, req.headers, req.body['headers']);

        const options: request.Options = {
            method: req.body['method'] || req.method,
            headers: headers,
            uri: this.config.synapse.endpoint + req.body['route'],
            body: req.body['data'],
            json: true,
            rejectUnauthorized: false
        };

        request(options).pipe(res)
    } */


    /**
     * Patch Users Webhook Fix.
     */
    triggerPatchUsers = async (req, res) => {

        try {
            const usersSnapshot = await this.app.database().ref('/users').once('value');
            const users = usersSnapshot.val();
            console.log(users);
            for (let user in users) {
                console.log(user);
                const context = { auth: { uid: user }, rawRequest: { ip: '127.0.0.1' } };

                let suid
                if (users[user] && users[user].sps && users[user].sps.id) {
                    suid = users[user].sps.id;
                } else {
                    console.log("missing SPSP ID:", user);
                    continue;
                }

                console.log(suid);
                const userData = await new SynapseFlowController(this.app, this.config).getUserSynapsePatch(user, suid);
                console.log(userData);
                const oauthData = await new SynapseFlowController(this.app, this.config).oauthPatch(userData, user, suid);
                console.log(oauthData);
                const fixResult = await new SynapseFlowController(this.app, this.config).patchFix(suid, user, oauthData.oauth_key);
                console.log(fixResult);
            }

            res.send(200)

        } catch (error) {
            res.send(400)
            this.handleError(error, 'PatchUsers');
            return error;
        }
    }


    /**
     * Create All required SPS nodes and user account.
     */
    createAccount = (data: any, context) => {

        let userRes, oauthRes;

        return new SynapseFlowController(this.app, this.config).create(data, context)
            .then((userData) => {
                userRes = userData
                return new SynapseFlowController(this.app, this.config).oauth(userData, context);
            })
            .then((oauth) => {
                oauthRes = oauth;
                return new SynapseFlowController(this.app, this.config).createSavingNode(oauth, context);
            })
            .then((node) => {
                return { user: userRes, oauth: oauthRes, node: node };
            })
            .catch((error) => {
                this.handleError(error, 'CreateAccount');
                return error;
            })
    }

    /**
     * Retrieve OAuth token.
     * todo: pass data to thenable
     */
    createToken = (data: any, context) => {

        return new SynapseFlowController(this.app, this.config).getUserInfo(data, context)
            .catch((error) => this.handleError(error, 'CreateToken: User Data'))
            .then((userData) => {
                console.log(userData);
                return new SynapseFlowController(this.app, this.config).getAccessToken({ uid: '', full_dehydrate: 'yes' }, context);
            })
            .catch((error) => this.handleError(error, 'CreateToken: Refresh token + OAuth'))
            .then((oauthData) => {
                return oauthData
            })
    }


    /**
     * Log errors to console & store to database.
     */
    handleError = (error, name?) => {
        new DBLogger(this.app.database()).error(name || 'SynapseController', error);
        console.error(name || 'SynapseController', error);
    }

}