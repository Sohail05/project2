import * as admin from 'firebase-admin';
import { CallableContext } from "firebase-functions/lib/providers/https";
import { DBLogger } from '../services/db-log';
import { configInterface } from '../interface/config';
import { DataSnapshot } from 'firebase-functions/lib/providers/database';
import { Onward } from "../../../shared/src/types/types";
import { GoalDTO } from '../../../shared/src/dto/goal.dto';

function pick(o, props) {
    return Object.assign({}, ...props.map(prop => {
        if (o[prop]) {
            return { [prop]: o[prop] }
        }
        return ''
    }));
}


export class GoalController {

    constructor(private app: admin.app.App, private config: configInterface) { }

    /**
     * Setup default goal "Emergency Funds".
     * Todo Add goal limit check;
     */
    createGoal: (data: GoalDTO, context: CallableContext) => any | Promise<any> = (data, context) => {

        if (!context.auth.uid) {
            return 'Unauthorized';
        }

        console.log('Setup new Goal:' + '/savings/' + context.auth.uid + '/goals');

        const goal = {
            active: true,
            amount: 0,
            createdAt: new Date().toISOString(),
            updatedAt: new Date().toISOString(),
            cycles: 0,
            deduction: data.deduction,
            default: false,
            name: data.name || 'Saving Goal',
            status: '',
            targetAmount: data.targetAmount,
            targetDate: data.targetDate,
        }

        return this.app.database().ref('/savings/' + context.auth.uid + '/dirty').set(true)
            .then((flagged) => {
                return this.app.database().ref('/savings/' + context.auth.uid + '/goals').push(goal).then((result: admin.database.Reference) => {
                    return { key: result.key }
                })
            })

    }

    editGoal: (data: GoalDTO, context: CallableContext) => any | Promise<any> = (data, context) => {

        if (!context.auth.uid) {
            return 'Unauthorized';
        }

        if (!data.gid) {
            throw new Error('Missing gid');
        }

        const payload = pick(data, ['deduction', 'name', 'targetAmount', 'targetDate'])
        const goal = Object.assign(
            payload,
            {
                updatedAt: new Date().toISOString(),
            })

        console.log('Goal update :' + data.gid, goal);

        return this.app.database().ref('/savings/' + context.auth.uid + '/goals/' + data.gid).update(goal)
            .then((result) => {
                return this.app.database().ref('/savings/' + context.auth.uid + '/dirty').set(true)
                    .then((flagged) => {
                        return this.app.database().ref('/savings/' + context.auth.uid + '/goals/' + data.gid).once('value')
                            .then((snapshot) => { return snapshot.val() });
                    })

            })

    }

    removeGoal: (data: { gid: string }, context: CallableContext) => any | Promise<any> = (data, context) => {

        if (!context.auth.uid) {
            return 'unauthorized';
        }

        if (!data.gid) {
            throw new Error('Missing gid');
        }

        return this.app.database().ref('/savings/' + context.auth.uid + '/dirty').set(true)
            .then((flagged) => {

                return this.app.database().ref('/savings/' + context.auth.uid + '/goals/' + data.gid).remove(
                    (error) => {
                        this.handleError(error, 'Remove Goal');
                    }).then(() => {
                        return 'success'
                    })
            })
    }

    /**
     * Log errors to console & store to database.
     */
    handleError = (error, name?) => {
        new DBLogger(this.app.database()).error(name || 'OnwardController', error);
        console.error(name || 'OnwardController', error);
    }

    /**
     * Setup default goal "Emergency Funds".
     * Todo: update only specific values.
     * Remmoval TBD
     */
    setupEmergencyFunds: (data: Onward.Goal, context: CallableContext) => any | Promise<any> = (data, context) => {

        if (!context.auth.uid) {
            return 'Unauthorized';
        }

        console.log('Setup Default Goal:' + '/savings/' + context.auth.uid + '/goals/default');
        console.log(data)

        const goal = {
            targetAmount: data.targetAmount,
            targetDate: data.targetDate,
            deduction: data.deduction,
            updatedAt: new Date().toISOString(),
            active: true
        }

        return this.app.database().ref('/savings/' + context.auth.uid + '/goals/default').update(goal).then((result) => {
            return this.app.database().ref('/savings/' + context.auth.uid + '/goals/default').once('value')
                .then((snapshot) => { return snapshot.val() });
        })

    }

}