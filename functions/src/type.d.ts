
/**
 * Request format on callable function to google functions.
 */
type SynapseRequest = {
    method?: string,
    route: string,
    data?: any
};

declare module synapsepay {}