import { SynapseRequestHeader } from "../classes/synapse-request-header";
import { configInterface } from "../interface/config";
import * as admin from 'firebase-admin';
import { Request, Response } from 'express';
import { DBLogger } from "../services/db-log";
import * as request from 'request-promise';
import { FlowController } from "../controllers/flow.controller";
import { SynapseApiUser, SynapseAuthPostResultDTO } from "../../../shared/src";

export class SynapseUserService {

    headers: SynapseRequestHeader;

    constructor(private app: admin.app.App, private config: configInterface) {
        this.headers = new SynapseRequestHeader(this.config.synapse);
    }

    /**
     * Controller HTTP Request handler.
     */
    requestHandler = (options: request.Options) => {
        const opt = Object.assign({ headers: this.headers.headers(), json: true }, options);
        return request(opt).catch((error) => { new DBLogger(this.app.database()).error(options['uri'], { error, options }); console.error(options['uri'], error); return error })
    }

    getUser = async (oid: string, suid: string): Promise<SynapseApiUser> => {

        if(!oid){
            throw Error("Missing oid");
        }
        
        if(!suid){
            throw Error("Missing suid");
        }

        this.headers.setIP('127.0.0.1');
        this.headers.setUser('|' + new FlowController().generateFingerprint(oid));

        const options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users/' + suid,
        }

        return this.requestHandler(options);
    }

    getOAuth = async (oid: string, suid: string, refresh_token: string): Promise<SynapseAuthPostResultDTO> => {

        if(!oid){
            throw Error("Missing oid");
        }
        
        if(!suid){
            throw Error("Missing suid");
        }

        if(!refresh_token){
            throw Error("Missing refresh_token");
        }

        this.headers.setIP('127.0.0.1');
        this.headers.setUser('|' + new FlowController().generateFingerprint(oid));

        const options = {
            method: 'post',
            uri: this.config.synapse.endpoint + 'oauth/' + suid,
            body: { refresh_token: refresh_token }
        }

        return this.requestHandler(options);
    }



}
