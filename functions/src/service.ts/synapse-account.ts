import { SynapseRequestHeader } from "../classes/synapse-request-header";
import { configInterface } from "../interface/config";
import * as admin from 'firebase-admin';
import { DBLogger } from "../services/db-log";
import * as request from 'request-promise';
import { FlowController } from "../controllers/flow.controller";
import { SynapseApiUser, SynapseAuthPostResultDTO } from "../../../shared/src";

export class SynapseAccountService {

    headers: SynapseRequestHeader;

    constructor(private app: admin.app.App, private config: configInterface) {
        this.headers = new SynapseRequestHeader(this.config.synapse);
    }

    /**
     * Controller HTTP Request handler.
     */
    requestHandler = (options: request.Options) => {
        const opt = Object.assign({ headers: this.headers.headers(), json: true }, options);
        return request(opt).catch((error) => { new DBLogger(this.app.database()).error(options['uri'], { error, options }); console.error(options['uri'], error); return error })
    }


    /**
     * Todo: type
     */
    getNode = async (oid: string, suid: string, nid: string, oauth_key: string): Promise<any> => {

        if (!oid) {
            throw Error("Missing oid");
        }

        if (!suid) {
            throw Error("Missing suid");
        }

        if (!nid) {
            throw Error("Missing nid");
        }

        if (!oauth_key) {
            throw Error("Missing oauth_key");
        }

        this.headers.setIP('127.0.0.1');
        this.headers.setUser(oauth_key + '|' + new FlowController().generateFingerprint(oid));

        const options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users/' + suid + '/nodes/' + nid,
        }

        return this.requestHandler(options);
    }

    /**
     * Todo: type
     */
    getSavingNodes = async (oid: string, suid: string, nid: string, oauth_key: string): Promise<any> => {

        if (!oid) {
            throw Error("Missing oid");
        }

        if (!suid) {
            throw Error("Missing suid");
        }

        if (!nid) {
            throw Error("Missing nid");
        }

        if (!oauth_key) {
            throw Error("Missing oauth_key");
        }

        this.headers.setIP('127.0.0.1');
        this.headers.setUser(oauth_key + '|' + new FlowController().generateFingerprint(oid));

        const options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users/' + suid + '/nodes/' + nid + '?type=IB-DEPOSIT-US',
        }

        return this.requestHandler(options);
    }

    /**
     * Todo: type
     */
    getSubnets = async (oid: string, suid: string, nid: string, oauth_key: string): Promise<any> => {

        if (!oid) {
            throw Error("Missing oid");
        }

        if (!suid) {
            throw Error("Missing suid");
        }

        if (!nid) {
            throw Error("Missing nid");
        }

        if (!oauth_key) {
            throw Error("Missing oauth_key");
        }


        this.headers.setIP('127.0.0.1');
        this.headers.setUser(oauth_key + '|' + new FlowController().generateFingerprint(oid));

        const options = {
            method: 'get',
            uri: this.config.synapse.endpoint + 'users/' + suid + '/nodes/' + nid + '/subnets',
        }

        return this.requestHandler(options);
    }



}
