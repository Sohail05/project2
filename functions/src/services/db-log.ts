import * as admin from 'firebase-admin';
import { LogService } from '../interface/log-service';

export class DBLogger implements LogService {

    ref = '/logs';

    constructor(public db: admin.database.Database) { }

    /**
     * Log data.
     * @param arg1 
     * @param arg2 
     */
    log(arg1: any, arg2: any): void {
        this.db.ref(this.ref + '/log').push({ arg1, arg2 });
    }

    /**
     * Logs incoming data received by cloud function.
     * @param fnName 
     * @param data
     */
    input(fnName: string, data: object): void {
        this.db.ref(this.ref + '/input').push({ fn: fnName, data });
    }
    /**
     * Log error encountered by cloud functions.
     * @param fnName 
     * @param data 
     */
    error(fnName: string, data: object): void {
        this.db.ref(this.ref + '/error').push({ fn: fnName, data: JSON.parse(JSON.stringify(data)) });
    }

}