import { NotificationController } from '../controllers/notification.controller';
import { FlowController } from '../controllers/flow.controller';
import * as admin from 'firebase-admin';
export class CustomerDatabaseTrigger {

    constructor(private app: admin.app.App, private config) { }

    /**
     * Create Customer trigger that generates the referralCode.
     */
    newCustomerCreated = (snapshot, context) => {
        const customer = snapshot.val();
        const referralCode = new FlowController().generateReferralCode(customer);
        console.log('Generating Referral Code', context.params.customer, referralCode);
        return snapshot.ref.child('referralCode').set(referralCode).then(() => {
            this.app.database().ref('/referralCodes/').push({ id: context.params.customer, referralCode: referralCode });
        });
    }

    newInvitationCreated = (snapshot, context) => {
        const invitation = snapshot.val();
        console.log(snapshot, context);
        console.log('Sending Invitation Notifications', context.params.customer, invitation);
        return snapshot.ref.child('status').set('sent').then(() => {
            const notification = new NotificationController(this.config);
            return notification.sendSMS({ phone: invitation.phone, body: "Onward Referral Code:" })
        });
    }


    newEmployeeTrigger = async (snapshot, context) => {

        const customerId = context.params.customer;
        const groupId = context.params.group;
        const employeeId = context.params.employee;
        const employeeData = snapshot.val();

        try {
            const customersSnap = await this.app.database().ref('customers/' + customerId).once("value");
            const customersData = customersSnap.val();

            const refCode = [
                customersData['code'] || customerId,
                customersData['groups'][groupId]['code'] || groupId,
                (customersData['groups'][groupId]['employees'][employeeId]['code'] || employeeId).substr(-4),
            ].join('').toLowerCase();

            const frequency = employeeData['frequency'] || customersData['groups'][groupId]['frequency'] || customersData['frequency']

            const refData = {
                referralCode: refCode,
                frequency: frequency,
                employerId: customerId,
                groupId: groupId,
                employeeId: employeeId,
                active: false,
                created_at: new Date().toISOString()
            }

            this.app.database().ref('referralCodes/' + refCode).set(refData);
            console.log("Created Referral Code : " + refCode);

        } catch (e) {
            console.error(e)
        }

        return
    }

}