import * as admin from 'firebase-admin';
import { Onward } from './../../../shared/src/types/types';
import { SynapseFlowController } from '../controllers/synapse-flow.controller';
import { NotificationController } from '../controllers/notification.controller';
import { SynapseUserService } from '../service.ts/synapse-user';
export class SavingDatabaseTrigger {

    constructor(private app: admin.app.App, private config) { }

    /**
     *  Create Default Saving Node "Emergency Fund".
     */
    SetupSavings = (snapshot, context) => {

        // User Node Object
        const user: Onward.user = snapshot.val();

        // User ID
        const userID = context.params.user
        console.log('Generating Saving Node for' + userID);
        
        /** 
        const goal: Onward.Goal = {
            name: 'Emergency Fund',
            createdAt: new Date().toISOString(),
            default: true,
            active: false,
            amount: 0,
            targetAmount: 0,
            targetDate: '',
            deduction: 0,
            status: '',
            cycles: 0
        }*/

        const saving = {
            createdAt: new Date().toISOString(),
            goals: [],
            employerId: user['employerId'],
            referralCode: user['referralCode'],
            totalDeduction: 0,
            cycles: 0,
        };

        return this.app.database().ref('savings/' + userID).set(saving);
    }



    /**
     * recalculate TotalDeduction on goal changes.
     */
    deductionUpdate = async (snapshot, context) => {

        const user = context.params.user;
        const userDataSnapshot = await this.app.database().ref('/savings/' + user).once('value');

        const goals = userDataSnapshot.val()['goals'];

        let sum = 0.00;
        for (const key in goals) {
            if (typeof goals[key].deduction === 'number') {
                sum = sum + goals[key].deduction;
            } else {
                sum = sum + Number.parseFloat(<any>goals[key].deduction);
            }
        }

        return this.app.database().ref('/savings/' + user).update({ totalDeduction: sum });

    }

    /**
     * The IB Node has been created on synapse.
     */
    newSavingCreated = async (snapshot, context) => {

        try {
            const userId = context.params.user;
            const userDataSnapshot = await this.app.database().ref('/users/' + userId).once('value');
            const userData: Onward.user = userDataSnapshot.val();

            new NotificationController(this.config).sendWelcomeNotification(userData.first_name, userData.email, userData.phone_number);
            const savingAccount: { id, balance, oauth_key } = snapshot.val();

            this.app.database().ref('savings/' + userId).update({ id: savingAccount.id });

            console.log('Generating Subnet Node for ' + userId + " " + savingAccount['id']);
            await this.app.database().ref('referralCodes/' + userData.referralCode).update({ active: false, used_at: new Date().toISOString() })

                        
            const spsSnap = await this.app.database().ref('/users/' + userId + '/sps').once('value')
            const spsData = spsSnap.val()
            
            const userResponse = await new SynapseUserService(this.app, this.config).getUser(userId, spsData.id);
            console.log("User Response", userResponse);
            const oauthResponse = await new SynapseUserService(this.app, this.config).getOAuth(userId, spsData.id, userResponse.refresh_token);
            console.log("OAuth Response", oauthResponse);

            const subnetData = { ouid: userId, suid: spsData.id, nid: savingAccount.id, oauth_key: oauthResponse.oauth_key };
            await new SynapseFlowController(this.app, this.config).createSubnet(subnetData, context);

        } catch (error) {

            console.log(error);

        }

    }

    /**
     * Distribute deductions amoung goals.
     */
    processPayroll = (snapshot, context) => {

        const params: { user: string, prId: string } = context.params;
        const payroll = snapshot.val() as Onward.Payroll;

        return this.app.database().ref('/savings/' + params.user).once('value', (snap) => {
            const savings = snap.val() as Onward.Savings;

            let payrollAmount = payroll.amount;
            for (const key in savings.goals) {
                if (payrollAmount < 0) {
                    break
                }

                const deduction: any = savings.goals[key].deduction;
                if (deduction !== 'number') {
                    savings.goals[key].deduction = Number.parseFloat(deduction as string)
                }

                if (payrollAmount >= savings.goals[key].deduction) {
                    savings.goals[key].amount = savings.goals[key].amount + savings.goals[key].deduction;
                    payrollAmount = payrollAmount - savings.goals[key].deduction;
                } else {
                    savings.goals[key].amount = Number.parseFloat(payrollAmount.toFixed(2));
                    break;
                }

            }

            savings.cycles = savings.cycles + 1;
            savings['updatedAt'] = new Date().toISOString();

            this.app.database().ref('/savings/' + params.user).update(savings);

        })

    }



}