
import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';

import { SynapseController } from './controllers/synapse.controller';
import { OnwardController } from './controllers/onward.controller';
import { CustomerDatabaseTrigger } from './database/customer.database';
import { SynapseFlowController } from './controllers/synapse-flow.controller';
import { DBLogger } from './services/db-log';
import { FlowController } from './controllers/flow.controller';
import { configInterface } from './interface/config';
import { SavingDatabaseTrigger } from './database/saving.database';
import { GoalController } from './controllers/goal.controller';
import { SynapseSyncController } from './controllers/synapse-sync.controller';

const config = functions.config() as configInterface;
const app = admin.initializeApp(JSON.parse(process.env.FIREBASE_CONFIG || "{}"));

/**
 * Middlewares.
 */

const injectLog = (data, context, obj, fnName) => {
    if (config['log'] && config['log'] === true) {
        new DBLogger(app.database()).input(fnName, data)
    }
    return obj[fnName](data, context);
}

const webhookSignature = (req, res, obj, fnName) => {

    const sha1Signature = req.get('X-Synapse-Signature');
    const sha256Signature = req.get('X-Synapse-Signature-Sha256');

    if (!req['body'] || !sha1Signature || !sha256Signature) {
        res.sendStatus(400);
        return;
    }

    const sha1 = new FlowController().generateSignature(req.body['_id']['$oid'], 'sha1', config.synapse);
    const sha256 = new FlowController().generateSignature(req.body['_id']['$oid'], 'sha256', config.synapse);

    if (sha1 !== sha1Signature || sha256 !== sha256Signature) {
        res.sendStatus(400);
        return;
    }

    return obj[fnName](req, res);
}

/*************************************** HTTP Triggers ******************************************/

export const usersListener = functions.https.onRequest((req, res) => {
    return webhookSignature(req, res, new SynapseController(app, config), 'usersListener');
})

export const nodesListener = functions.https.onRequest((req, res) => {
    return webhookSignature(req, res, new SynapseController(app, config), 'nodesListener');
})

export const transactionsListener = functions.https.onRequest((req, res) => {
    return webhookSignature(req, res, new SynapseController(app, config), 'transactionsListener');
})

export const triggerSynapseUserPatch = functions.https.onRequest(new SynapseController(app, config).triggerPatchUsers)
export const payroll = functions.https.onRequest(new OnwardController(app, config).payroll)
export const payrollArchive = functions.https.onRequest(new OnwardController(app, config).payrollArchive)

export const errorHandler = functions.https.onRequest((req, res) => {
    app.database().ref('/fe/errors').push(req.body);
    res.send(200);
})


/*************************************** Database Triggers ******************************************/

//export const newCustomerTrigger = functions.database.ref('customers/{customer}').onCreate(new CustomerDatabaseTrigger(app, config).newCustomerCreated);
export const newEmployeeTrigger = functions.database.ref('/customers/{customer}/groups/{group}/employees/{employee}').onCreate(new CustomerDatabaseTrigger(app, config).newEmployeeTrigger);
export const newInvitationTrigger = functions.database.ref('/customers/{customer}/invites/{invite}').onCreate(new CustomerDatabaseTrigger(app, config).newInvitationCreated);
export const newUserTrigger = functions.database.ref('/users/{user}').onCreate(new SavingDatabaseTrigger(app, config).SetupSavings);
export const newSavingTrigger = functions.database.ref('/users/{user}/sps/saving').onCreate(new SavingDatabaseTrigger(app, config).newSavingCreated);
export const processPayroll = functions.database.ref('/savings/{user}/payroll/{prId}').onCreate(new SavingDatabaseTrigger(app, config).processPayroll);
export const deductionUpdate = functions.database.ref('/savings/{user}/goals/{goalId}/deduction').onWrite((new SavingDatabaseTrigger(app, config).deductionUpdate));

export const testTrigger = functions.database.ref('/{something}').onCreate(
    (snapshot, context) => {
        console.log("Created something: " + context.params['something']);
    }
);
/*************************************** Callable Functions ******************************************/

// Account Opening

export const referralCodeLookUp = functions.https.onCall(new OnwardController(app, config).referralCodeLookUp);
export const employerRequest = functions.https.onCall(new OnwardController(app, config).employerRequest);
export const createCustomer = functions.https.onCall(new OnwardController(app, config).createCustomer);
export const createUser = functions.https.onCall(new OnwardController(app, config).createUser);
export const spsCreateAccount = functions.https.onCall(new SynapseController(app, config).createAccount);
export const spsCreateUser = functions.https.onCall(new SynapseFlowController(app, config).create);
export const accountClosureRequest = functions.https.onCall(new OnwardController(app, config).accountClosureRequest);

// Goals Controller

export const setupEmergencyFunds = functions.https.onCall(new GoalController(app, config).setupEmergencyFunds);
export const createGoal = functions.https.onCall(new GoalController(app, config).createGoal);
export const editGoal = functions.https.onCall(new GoalController(app, config).editGoal);
export const removeGoal = functions.https.onCall(new GoalController(app, config).removeGoal);

// Home & Dashboard

export const availableFunds = functions.https.onCall(new OnwardController(app, config).availableFunds);

// Education 

export const tips = functions.https.onCall(new OnwardController(app, config).tips);

// Synapse

export const createAchNode = functions.https.onCall(new SynapseFlowController(app, config).createAchNode);
export const deleteAchNode = functions.https.onCall(new SynapseFlowController(app, config).deleteAchNode);
export const getAchNodes = functions.https.onCall(new SynapseFlowController(app, config).getAchNodes);
export const getAccessToken = functions.https.onCall(new SynapseFlowController(app, config).getAccessToken);

export const verifyAch = functions.https.onCall((data, context) => {
    return injectLog(data, context, new SynapseFlowController(app, config), "verifyAch")
});

export const withdrawalTransaction = functions.https.onCall((data, context) => {
    return injectLog(data, context, new SynapseFlowController(app, config), "withdrawalTransaction")
});

export const transactionHistory = functions.https.onCall((data, context) => {
    return injectLog(data, context, new SynapseFlowController(app, config), "transactionHistory")
});

export const spsGetUsers = functions.https.onCall(new SynapseFlowController(app, config).findAll);
export const spsGetUser = functions.https.onCall(new SynapseFlowController(app, config).find);
export const spsUpdateUser = functions.https.onCall(new SynapseFlowController(app, config).update);
export const dummyTransaction = functions.https.onCall(new SynapseFlowController(app, config).dummyTransaction);
export const balance = functions.https.onCall(new SynapseFlowController(app, config).balance);

/**
 * Synapse Sync Functions.
 */

export const syncUser = functions.https.onRequest(new SynapseSyncController(app, config).syncUser);
export const syncNode = functions.https.onRequest(new SynapseSyncController(app, config).syncNode);
export const syncSubnet = functions.https.onRequest(new SynapseSyncController(app, config).syncSubnet);
export const syncAll = functions.https.onRequest(new SynapseSyncController(app, config).syncAll);

/**
 * Employer Portal.
 */

export const employerMasterList = functions.https.onCall(new OnwardController(app, config).masterList)
export const employerFileList = functions.https.onCall(new OnwardController(app, config).employerFileList)