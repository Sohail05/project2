export class ClassHelper {
    static getMethodName(obj, method) {
        let methodName = null;
        Object.getOwnPropertyNames(obj).forEach(prop => {
            if (obj[prop] === method) {
                methodName = prop;
            }
        });

        if (methodName !== null) {
            return methodName;
        }

        const proto = Object.getPrototypeOf(obj);
        if (proto) {
            return ClassHelper.getMethodName(proto, method);
        }
        return null;
    }
}