import * as net from 'net';

export class SynapseRequestHeader {

    protected 'X-SP-GATEWAY': string;
    protected 'X-SP-USER-IP': string;
    protected 'X-SP-USER' = '';

    constructor(client: { client_id: string, client_secret: string }) {
        this.setGateway(client);
    }

    /**
     * Set synapse client id & secret header.
     * @param client
     */
    setGateway(client: { client_id: string, client_secret: string }) {
        this["X-SP-GATEWAY"] = [client.client_id, client.client_secret].join('|');
    }

    /**
     * Set client origin IP header.
     * @param value 
     */
    setIP(value: string) {
        if (net.isIP(value) > 0) {
            this["X-SP-USER-IP"] = value;
        } else {
            this["X-SP-USER-IP"] = '0.0.0.0';
        }
    }

    /**
     * Set user oauth & fingerprint header.
     * @param value 
     */
    setUser(value: string) {
        this["X-SP-USER"] = value;
    }

    /**
     * Get JSON format headers.
     * @param value 
     */
    headers() {
        return {
            'X-SP-GATEWAY': this["X-SP-GATEWAY"],
            'X-SP-USER-IP': this["X-SP-USER-IP"],
            'X-SP-USER': this["X-SP-USER"],
        }
    }

}