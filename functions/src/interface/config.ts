export interface configInterface {
    "synapse": {
        "endpoint": string,
        "client_secret": string,
        "user_id": string,
        "node_id": string,
        "client_id": string,
    },
    "gmail": {
        "email": string,
        "password": string
    },
    "twilio": {
        "auth_token": string,
        "sid": string,
    }
}
