export interface LogService {
    
    /**
     * Log passed arguments.
     * @param arg1 
     * @param arg2 
     */
    log(arg1: any, arg2: any): void;
    
    /**
     * Logs data received by cloud function.
     * @param fnName 
     * @param data 
     */
    input(fnName:string, data: object): void;
    /**
     * Log error encountered by cloud functions.
     * @param fnName 
     * @param data 
     */
    error(fnName:string, data: object): void;
}