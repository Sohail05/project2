

export const welcomeContent = (name: string) => { 
   
return `Hi ${name}! 

Congratulations! 
You are now on your way to building a financial cushion and achieving your goals.
`
}