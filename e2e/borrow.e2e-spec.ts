///<reference path="./../typings/globals/jasmine/index.d.ts"/>
import { element, browser, by, protractor } from 'protractor';

describe('Borrow page landing scenarios', function() {
  function loginUser(email, password, done) {
    browser.get('/signin');
    browser.driver.findElement(by.id('email')).sendKeys(email);
    browser.driver.findElement(by.id('password')).sendKeys(password);
    browser.driver.executeScript('arguments[0].scrollIntoView(true);',
    browser.driver.findElement(by.id('submit')))
      .then(function() {
        browser.driver.findElement(by.id('submit')).click();
        browser.driver.wait(function() {
          return browser.driver.getCurrentUrl().then(function(url) {
            browser.driver.sleep(5000);
            return true;
          });
        }, 5000);
      });
  }

  function logout(done) {
    browser.driver.executeScript('arguments[0].scrollIntoView(true);',
      browser.driver.findElement(by.id('logout')))
      .then(function() {
        browser.driver.findElement(by.id('logout')).click();
        browser.driver.wait(function() {
          return browser.driver.getCurrentUrl().then(function(url) {
            return /login/.test(url);
          });
        }, 5000);
      });
      done();
  }
  beforeAll(function(done) {
    loginUser('xtreem88@gmail.com', 'letmein', done);
    done();
  });

  afterAll(function(done){
    logout(done);
    done();
  });

  describe('Request loan page', function() {
    beforeEach(function(done) {
      browser.ignoreSynchronization = true;
      browser.get('/#/borrow/request');
      done();
    });

    it('should have main', () => {
      // browser.driver.getCurrentUrl().then(function(url) {console.log(url);});
      browser.driver.sleep(5000);
      const elem = element(by.css('#main'));
      expect(elem.isPresent()).toBeTruthy();
    });

    it('should have the right title', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#title'));
      expect(elem.isPresent()).toBeTruthy();
      expect(elem.getText()).toEqual('Request Onward Loan');
    });
    it('should have grey exit button', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#exit'));
      expect(elem.getText()).toEqual('EXIT');
      expect(elem.getCssValue('background-color')).toEqual('rgba(153, 153, 153, 1)');
    });
    it('should have blue next button', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#next'));
      expect(elem.getText()).toEqual('NEXT');
      expect(elem.isEnabled()).toBeFalsy();
      expect(elem.getCssValue('background-color')).toEqual('rgba(33, 150, 243, 1)');
    });
    it('should disable next button if step is not valid', () => {
      browser.driver.sleep(5000);
      let elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeFalsy();
      browser.driver.findElement(by.id('amount')).sendKeys(200);
      element(by.cssContainingText('option', '3 months')).click();
      element(by.cssContainingText('option', 'Repair a car')).click();
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeTruthy();
      browser.driver.findElement(by.id('next')).click();
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeTruthy();
      browser.driver.findElement(by.id('next')).click();
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeFalsy();
      browser.driver.findElement(by.id('accountNo')).sendKeys(12345);
      browser.driver.findElement(by.id('routingNo')).sendKeys(123456789);
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeTruthy();
      browser.driver.findElement(by.id('next')).click();
      expect(element(by.css('#amount-summary')).getAttribute('value')).toEqual('$200.00');
      expect(element(by.css('#interest-summary')).getAttribute('value')).toEqual('$9.00');
      expect(element(by.css('#total-summary')).getAttribute('value')).toEqual('$209.00');
      expect(element(by.css('#payment-summary')).getAttribute('value')).toEqual('$34.83');
    });
    it('should have back button', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('amount')).sendKeys(200);
      element(by.cssContainingText('option', '3 months')).click();
      element(by.cssContainingText('option', 'Repair a car')).click();
      browser.driver.findElement(by.id('next')).click();
      const elem = element(by.css('#back'));
      expect(elem.getText()).toEqual('BACK');
    });

    it('should validate amount field', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#amount'));
      expect(elem.isDisplayed()).toBeTruthy();
      browser.driver.findElement(by.id('amount'))
      .then(function() {
        browser.driver.findElement(by.id('amount')).sendKeys('text');
        const elemt = element(by.css('#amount-error'));
        expect(elemt.getText()).toEqual('Amount should be numbers only and greater than zero');
      });
    });
    it('should allow correct amount', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('amount'))
      .then(function() {
        browser.driver.findElement(by.id('amount')).sendKeys(200);
        const elemt = element(by.css('#amount-error'));
        expect(browser.isElementPresent(elemt)).toBe(false);
      });
    });
    it('should confirm before exit', () => {
      browser.driver.sleep(5000);
      let elemt;
      browser.driver.findElement(by.id('exit'))
      .then(function() {
        browser.driver.findElement(by.id('exit')).click();
        browser.driver.sleep(2000);
        elemt = element(by.css('#leave'));
        expect(elemt.isDisplayed()).toBeTruthy();
      });
      browser.driver.findElement(by.id('leave')).click();
      browser.driver.sleep(2000);
      browser.driver.getCurrentUrl().then(function(url) {
        expect(url).toMatch('/#/borrow');
        elemt = element(by.css('#totalSavings'));
        expect(elemt.isPresent()).toBeTruthy();
      });

    });
  });
});
