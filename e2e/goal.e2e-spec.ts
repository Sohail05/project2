///<reference path="./../typings/globals/jasmine/index.d.ts"/>
import { element, browser, by, protractor } from 'protractor';

describe('Goal page landing scenarios', function() {
  function loginUser(email, password, done) {
    browser.get('/#/signin');
    browser.driver.findElement(by.id('email')).sendKeys(email);
    browser.driver.findElement(by.id('password')).sendKeys(password);
    browser.driver.executeScript('arguments[0].scrollIntoView(true);',
    browser.driver.findElement(by.id('submit')))
      .then(function() {
        browser.driver.findElement(by.id('submit')).click();
        browser.driver.wait(function() {
          return browser.driver.getCurrentUrl().then(function(url) {
            browser.driver.sleep(5000);
            return true;
          });
        }, 5000);
      });
  }

  function logout(done) {
    browser.driver.executeScript('arguments[0].scrollIntoView(true);',
      browser.driver.findElement(by.id('logout')))
      .then(function() {
        browser.driver.findElement(by.id('logout')).click();
        browser.driver.wait(function() {
          return browser.driver.getCurrentUrl().then(function(url) {
            return /login/.test(url);
          });
        }, 5000);
      });
      done();
  }
  beforeAll(function(done) {
    loginUser('xtreem88@gmail.com', 'letmein', done);
    done();
  });

  afterAll(function(done){
    logout(done);
    done();
  });

  describe('Create goal page', function() {
    beforeEach(function(done) {
      browser.ignoreSynchronization = true;
      browser.get('/#/goal/add');
      done();
    });

    it('should have main', () => {
      // browser.driver.getCurrentUrl().then(function(url) {console.log(url);});
      browser.driver.sleep(5000);
      const elem = element(by.css('#main'));
      expect(elem.isPresent()).toBeTruthy();
    });

    it('should have the right title', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#title'));
      expect(elem.isPresent()).toBeTruthy();
      expect(elem.getText()).toEqual('Goals - Set Your Goal');
    });
    it('should have grey exit button', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#exit'));
      expect(elem.getText()).toEqual('EXIT');
      expect(elem.getCssValue('background-color')).toEqual('rgba(153, 153, 153, 1)');
    });
    it('should confirm before exit', () => {
      browser.driver.sleep(5000);
      let elemt = element(by.css('#leave'));
      expect(elemt.isDisplayed()).toBeFalsy();
      browser.driver.findElement(by.id('exit'))
      .then(function() {
        browser.driver.findElement(by.id('exit')).click();
        browser.driver.sleep(2000);
        elemt = element(by.css('#leave'));
        expect(elemt.isDisplayed()).toBeTruthy();
      });
      browser.driver.findElement(by.id('leave')).click();
      browser.driver.sleep(2000);
      browser.driver.getCurrentUrl().then(function(url) {
        expect(url).toMatch('/#/save');
        elemt = element(by.css('#goalTracker'));
        expect(elemt.isPresent()).toBeTruthy();
      });
    });

    it('should have blue next button', () => {
      browser.driver.sleep(5000);
      const elem = element(by.css('#next'));
      expect(elem.getText()).toEqual('NEXT');
      expect(elem.isEnabled()).toBeFalsy();
      expect(elem.getCssValue('background-color')).toEqual('rgba(33, 150, 243, 1)');
    });
    it('should have back button', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('goal-title')).sendKeys('Buy a car');
      browser.driver.findElement(by.id('next')).click();
      const elem = element(by.css('#back'));
      expect(elem.getText()).toEqual('BACK');
    });
    it('should validate amount field', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('goal-title')).sendKeys('Buy a car');
      browser.driver.findElement(by.id('next')).click();
      const elem = element(by.css('#amount'));
      expect(elem.isDisplayed()).toBeTruthy();
      browser.driver.findElement(by.id('amount'))
      .then(function() {
        browser.driver.findElement(by.id('amount')).sendKeys('text');
        const elemt = element(by.css('#amount-error'));
        expect(elemt.getText()).toEqual('Please enter a number greater than 0');
      });
    });
    it('should allow correct amount', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('goal-title')).sendKeys('Buy a car');
      browser.driver.findElement(by.id('next')).click();
      browser.driver.findElement(by.id('amount'))
      .then(function() {
        browser.driver.findElement(by.id('amount')).sendKeys(200);
        const elemt = element(by.css('#amount-error'));
        expect(browser.isElementPresent(elemt)).toBe(false);
      });
    });
    it('should disable next button if step is not valid', () => {
      browser.driver.sleep(5000);
      let elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeFalsy();
      browser.driver.findElement(by.id('goal-title')).sendKeys('Buy a car');
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeTruthy();
      browser.driver.findElement(by.id('next')).click();
      browser.driver.findElement(by.id('amount')).sendKeys(200);
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeTruthy();
      browser.driver.findElement(by.id('next')).click();
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeFalsy();
      browser.driver.findElement(by.id('duration-3')).click();
      elem = element(by.css('#next'));
      expect(elem.isEnabled()).toBeTruthy();
      browser.driver.findElement(by.id('next')).click();
      browser.driver.findElement(by.id('next')).click();
      expect(element(by.css('#amount-summary')).getAttribute('value')).toEqual('$200.00');
      expect(element(by.css('#saving-summary')).getAttribute('value')).toEqual('$33.33');
    });
    it('should have the correct amount field names', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('goal-title')).sendKeys('Buy a car');
      browser.driver.findElement(by.id('next')).click();
      let elemt = element(by.css('#sugestion-title'));
      expect(elemt.getText()).toEqual('How much do you want to save towards your goal?');
      elemt = element(by.css('#suggested-amount'));
      expect(browser.isElementPresent(elemt)).toBe(true);
      elemt = element(by.css('#amount-100'));
      expect(elemt.getText()).toEqual('$100');
      elemt = element(by.css('#amount-200'));
      expect(elemt.getText()).toEqual('$200');
      elemt = element(by.css('#amount-500'));
      expect(elemt.getText()).toEqual('$500');
      elemt = element(by.css('#amount-1000'));
      expect(elemt.getText()).toEqual('$1000');
      browser.driver.findElement(by.id('amount-200')).click();
      expect(element(by.css('#amount')).getAttribute('value')).toEqual('200');
    });
    it('should have the correct field names for Recommended savings per paycheck and goal achievement date', () => {
      browser.driver.sleep(5000);
      browser.driver.findElement(by.id('goal-title')).sendKeys('Buy a car');
      browser.driver.findElement(by.id('next')).click();
      browser.driver.sleep(1000);
      browser.driver.findElement(by.id('amount')).sendKeys(200);
      browser.driver.findElement(by.id('next')).click();
      let elemt = element(by.css('#goal-duration-suggestion'));
      expect(elemt.getText()).toEqual('How long do you want to save for this goal?');
      elemt = element(by.css('#suggested-duration'));
      expect(browser.isElementPresent(elemt)).toBe(true);
      elemt = element(by.css('#duration-3'));
      expect(elemt.getText()).toEqual('3 Months');
      elemt = element(by.css('#duration-4'));
      expect(elemt.getText()).toEqual('4 Months');
      elemt = element(by.css('#duration-6'));
      expect(elemt.getText()).toEqual('6 Months');
      elemt = element(by.css('#duration-12'));
      expect(elemt.getText()).toEqual('12 Months');
      elemt = element(by.css('#goal-due-date'));
      expect(elemt.getText()).toEqual('I will reach my goal on:');
      elemt = element(by.css('#goal-recommended-saving'));
      expect(elemt.getText()).toEqual('Recommended savings per paycheck');
    });
  });
});
