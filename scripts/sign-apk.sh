#! bin/bash

cp ./platforms/android/app/build/outputs/apk/release/app-release-unsigned.apk ./
jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore onward-release-key.jks app-release-unsigned.apk onward -storepass onward_da39a3
rm OnwardFinancial.apk
zipalign -v 4 app-release-unsigned.apk OnwardFinancial.apk